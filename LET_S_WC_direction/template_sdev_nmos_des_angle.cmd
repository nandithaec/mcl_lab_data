*Quasi section changed to ramp gate1 to 1.2, unset command removed
**Extrapolated commented out - Nov 12 2015

Device TWO_NMOS_DEV {

Electrode {
  { Name="source1"        Voltage=0.0 }
  { Name="drain1"         Voltage=0.0 }
  { Name="gate1"          Voltage=0.0 }
  { Name="substrate"      Voltage=0.0 }
  { Name="source2"        Voltage=0.0 }
  { Name="drain2"         Voltage=0.0 }
  { Name="gate2"          Voltage=0.0 }

}


File {
  * Input Files
  Grid      = "../two_nmos/two_nmos_new_msh.tdr"
  Parameter = "../two_nmos/models.par"
  * Output Files
  Current = "nmos_layout_angle_#number#"
  Plot    = "nmos_layout_angle_#number#"
  
}


**DopingDependence 

Physics{
  eQCvanDort 
  AreaFactor=1.0
  EffectiveIntrinsicDensity(BandGapNarrowing (OldSlotboom))
  Mobility (CarrierCarrierScattering DopingDependence   HighFieldsaturation    Enormal )
  Recombination ( SRH Auger )
     

**Strike on 1st NMOS
**This section is within Physics section
  HeavyIon (
  Direction=(#xdir#,#ydir#)  * y direction at an angle of 45 deg
  Location=(#xlocation#,#ylocation#)  *(x,y) micrometer point where the heavy ion enters the device
  Time=60e-12  ** Time at which the ion penetrates the device.
  Length=#length#  *track length in micron
  Wt_hi=#radius#  *in microns
  LET_f=#LET#  *in picoColoumb per micrometer
  Gaussian   *spatial distribution as a Gaussian function
  PicoCoulomb 	)
}

} *End TWO_NMOS_DEV device




File{
   Output = "log_global_#number#"
}


Plot{
*--Density and Currents, etc
   eDensity hDensity


*--Temperature 
*   eTemperature Temperature * hTemperature

*--Fields and charges
   ElectricField/Vector Potential SpaceCharge

*--Doping Profiles
   Doping DonorConcentration AcceptorConcentration

*--Generation/Recombination
   SRH Band2Band * Auger
 *  AvalancheGeneration eAvalancheGeneration hAvalancheGeneration

*--Heavy Ion
  HeavyIonChargeDensity
  HeavyIonGeneration

}


System{



*Vdrain for NMOS
Vsource_pset drain_1 (drain1 0) { dc = #Vdrain1#}
Vsource_pset drain_2 (drain2 0) { dc = #Vdrain2#}

**Vin=Vgate
Vsource_pset vin (gate1 0) { dc =  0 } **ramp to 1.2
Vsource_pset vin2 (gate2 0) { dc = #Vgate2# }

**Vsource
Vsource_pset vs (source1 0) { dc = #Vsource1# }
Vsource_pset vs2 (source2 0) { dc = #Vsource2# }

**Vsubstrate
Vsource_pset vsub (subs 0) { dc = 0}

TWO_NMOS_DEV two_nmos ( "source1"=source1  "drain1"=drain1 "gate1"=gate1 "substrate"=subs "source2"=source2  "drain2"=drain2 "gate2"=gate2 )

**Capacitor_pset cout ( drain2 0 ){ capacitance = 0.001e-12 }

Plot "nmos2_ser_layout_angle_#number#.plt" (time() i(two_nmos,drain1) i(two_nmos,drain2) i(two_nmos,source1) i(two_nmos,source2))
 
  }  *End system



Math{

** Extrapolate
 RelErrControl
*Newton iterations converge best with full derivatives.
Derivatives
notdamped=50 
 Iterations=12
*Improved Alpha Particle/Heavy Ion Generation RateIntegration
 RecBoxIntegr
*Parallel, iterative linear solver
 Method=ILS
* Spice_gmin=1e-15
Transient=BE
}


Solve
**This is the only sequential section in this command file
{  
  NewCurrentPrefix="init"
  Coupled { Poisson }
 Coupled{ Poisson Electron Hole Contact Circuit}


 Quasistationary( 
     InitialStep=1e-3 Increment=1.35 
     MinStep=1e-5 MaxStep=0.05 
     Goal{ Parameter=vin.dc Voltage= 1.2 }
  ){ Coupled{ Poisson Electron Hole Contact Circuit}
  }

Save(FilePrefix="vddv")

**Transient simulation
Load(FilePrefix="vddv")
 NewCurrentPrefix=""
  Transient (
     InitialTime=0 FinalTime=250e-12
     InitialStep=1e-12 Increment=1.3
     MaxStep=5e-12 MinStep=1e-15
  ){ Coupled{ two_nmos.poisson two_nmos.electron two_nmos.hole two_nmos.contact 
           circuit }
  }

}



