#!/usr/bin/env python

#Feb 9 2015

#Example usage: python python_runboth_finfet.py  -t /home/users/nanditha/sentaurus/3d_finfet/6finfets/CAF  -f ser_6finfets_source -n 14 -s 1.6e-10  -g table.csv -i result_combined.csv

import optparse
import re,os
import csv, re
import random,shutil
import fileinput,sys


from optparse import OptionParser



parser = OptionParser('This script combines 2 csv files column wise\nAuthor:Nanditha Rao(nanditha@ee.iitb.ac.in)\n')

parser.add_option("-t", "--tem",dest='temp_path', help='Enter the path of the results files')
parser.add_option("-f", "--fil",dest='result', help='Enter the name of the results files')
parser.add_option("-n", "--num",dest='num', help='Enter the total number of results files ')
parser.add_option("-s", "--sim",dest='sim_time', help='Enter the final simulation time ')
parser.add_option("-g", "--fi1",dest='csv1', help='Enter the first csv file name (with csv) assuming its in the same  path')
parser.add_option("-i", "--fi2",dest='csv2', help='Enter the 2nd csv file name (with csv) assuming its in the same path')



(options, args) = parser.parse_args()


path=options.temp_path
number=options.num
result_file=options.result
sim_time=options.sim_time
csv1=options.csv1
csv2=options.csv2


os.system ("python python_post-process_250p_limit_finfet.py  -t %s -f %s -n %s -s  %s"  %(path,result_file,number,sim_time ))

os.system ("python python_combine_2csv.py  -t %s -g %s/%s -i %s/%s"  %(path,path,csv1,path,csv2))



