**Ramping vddp and vddp2, instead of Save and Load solution. Try the save and load option later.
**Having caps on both outputs
**Using Poisson Electron Hole Contact Circuit instead of inverter.Poisson etc
**Cleaned up comments

Device CMOS_INV {

Electrode {
  { Name="source1"        Voltage=0.0 }
  { Name="drain1"         Voltage=0.0 }
  { Name="gate1"          Voltage=0.0 }
  { Name="substrate"     Voltage=0.0 }

  { Name="source2"        Voltage=0.0 }
  { Name="nwell_pmos"     Voltage=0.0 }
  { Name="drain2"         Voltage=0.0 }
  { Name="gate2"          Voltage=0.0 }

 { Name="source1_i2"        Voltage=0.0 }
  { Name="drain1_i2"         Voltage=0.0 }
  { Name="gate1_i2"          Voltage=0.0 }
  { Name="substrate_i2"     Voltage=0.0 }

  { Name="source2_i2"        Voltage=0.0 }
  { Name="nwell_pmos_i2"     Voltage=0.0 }
  { Name="drain2_i2"         Voltage=0.0 }
  { Name="gate2_i2"     Voltage=0.0 }

}


File {
  * Input Files


   Grid      = "../cmos_2inverter_layout_D1-S2_body2_msh.tdr"
  Parameter = "../models.par"  
  * Output Files
  Current = "cmos_inv_layout_ser_body2_1"
  Plot    = "cmos_inv_layout_ser_body2_1"
  
}


**DopingDependence 

Physics{
*   eQCvanDort 
   EffectiveIntrinsicDensity( OldSlotboom )     
 Mobility (DopingDependence   HighFieldsaturation    Enormal )
  Recombination ( SRH Auger )


**Strike on NMOS
**This section is within Physics section
   HeavyIon (
  Direction=(0,1)  * y direction
  Location=(0.35,0.3,0.25)  *(x,y,z) micrometer point where the heavy ion enters the device
  Time=50e-12  ** Time at which the ion penetrates the device.
  Length=1.46  *track length in micron
  Wt_hi=0.27  *in microns
  LET_f=0.01  *in picoColoumb per micrometer
  Gaussian   *spatial distribution as a Gaussian function
  PicoCoulomb 	)
     
}

} *End  device




File{
   Output = "cmos_layout_inv_D1-S2_body2_1"
}


Plot{
*--Density and Currents, etc
   eDensity hDensity


*--Fields and charges
   ElectricField/Vector Potential SpaceCharge

*--Doping Profiles
   Doping DonorConcentration AcceptorConcentration


*--Heavy Ion
  HeavyIonChargeDensity
  HeavyIonGeneration

}


System{

Vsource_pset vddp (source_pmos 0) { dc = 0 } *ramp
Vsource_pset vss (source_nmos 0) { dc = 0 }
Vsource_pset vin (gate_in 0)  { dc = 0 }

**Vsubstrate
Vsource_pset vsub (subs 0) { dc = 0}

Vsource_pset vnwell (nwell_pmos 0) { dc = 1.2}

**Inv2
Vsource_pset vddp2 (source_pmos_i2 0) { dc = 0 } *ramp
Vsource_pset vss2 (source_nmos_i2 0) { dc = 0 }
Vsource_pset vin2 (gate_in_i2 0)  { dc = 0 }

**Vsubstrate
Vsource_pset vsub2 (subs_i2 0) { dc = 0}

Vsource_pset vnwell2 (nwell_pmos_i2 0) { dc = 1.2}




CMOS_INV inverter ( "source1"=0   "drain1"=drain_out  "gate1"=gate_in  "substrate"=subs "drain2"=drain_out   "source2"=source_pmos   "nwell_pmos"=nwell_pmos  "gate2"=gate_in  "source1_i2"=0  "drain1_i2"=drain_out_i2  "gate1_i2"=gate_in_i2  "substrate_i2"=subs_i2  "drain2_i2"=drain_out_i2  "source2_i2"=source_pmos_i2   "nwell_pmos_i2"=nwell_pmos_i2   "gate2_i2"=gate_in_i2)


**set (nwell_pmos=1.2)

 Capacitor_pset cout ( drain_out 0 ){ capacitance = 3e-16 }
 Capacitor_pset cout2 ( drain_out_i2 0 ){ capacitance = 3e-16 }

Plot "cmos_inv_layout_ser_body2_1.plt" (time()  i(inverter,drain_out)  i(inverter,drain_out_i2)  i(inverter,subs) i(inverter,subs_i2))

Plot "cmos_inv_layout_ser_body2_all_1.plt" (time()   v(gate_in) v(drain_out)   v(source_pmos) v(nwell_pmos)  v(source1) v(gate1)  v(gate2)  i(inverter,drain_out)  i(inverter,drain1)  i(inverter,drain2) i(inverter,source1)  i(inverter,source_pmos)  i(inverter,subs) i(inverter,nwell_pmos)  v(gate_in_i2)  v(drain_out_i2)   v(source_pmos_i2) v(nwell_pmos_i2) v(source1_i2) v(gate2_i2)  i(inverter,drain_out_i2)  i(inverter,drain1_i2) i(inverter,drain2_i2) i(inverter,source1_i2)  i(inverter,source_pmos_i2) i(inverter,subs_i2) i(inverter,nwell_pmos_i2))
 
  }  *End system




Math{
CheckTransientError
  Extrapolate
  RelErrControl
  Digits=4
  Notdamped=50
  Iterations=12
  Transient=BE
  Method=ILS
 * SubMethod=ParDiSo
*CNormPrint
*AcceptNewtonParameter ( RhsMin=1e-5 )

}


Solve
**This is the only sequential section in this command file
{  
 NewCurrentPrefix="init"
  Coupled(Iterations=20){ Poisson }
  Coupled{ Poisson Electron Hole Contact Circuit }


Quasistationary( 
     InitialStep=1e-3 Increment=1.35 
     MinStep=1e-5 MaxStep=0.08 
     Goal{ Parameter=vddp.dc Voltage= 1.2 }
    Goal{ Parameter=vddp2.dc Voltage= 1.2 }  
  ){ Coupled{ Poisson Electron Hole Contact Circuit }  
   }

** Save(FilePrefix="dc_D1-S2_body2_1")

**Transient simulation
**Load(FilePrefix="#dc_soln#")

  NewCurrentPrefix=""
  Transient (
     InitialTime=0 FinalTime=160e-12
    * InitialStep=0.5e-15 Increment=1.3 Decrement=1.3
    * MaxStep=5e-12 MinStep=1e-17
   InitialStep=1e-13 Increment=1.3
     MaxStep=6e-12 MinStep=1e-16

 ){ Coupled{ Poisson Electron Hole Contact Circuit }
  }

}




