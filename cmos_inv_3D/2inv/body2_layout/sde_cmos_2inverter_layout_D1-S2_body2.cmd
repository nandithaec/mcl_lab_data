;The working One-Inv cmd file taken, and Inv2 is duplicated afresh- 3 Jan 2016- latest file
;Body 2 layout
;Common n-well- Jan 12-->2.9lakh elements
;Body contact z: 0.1 for nmos and pmos, added substrate front, body contact width modified - Jan 12
;Increase Subs_start,Nwellstart Xwell reduced to 0.07, Hsubs to 1
;Channel mesh Xj/5-->Xj/4, 4e-4-->1e-3
;Active region- y- meshing changed
;Gpn 0.007, Subs, Gate, Channel meshing modified- mostly z direction - Jan 11 2016
; Tox, Substrate meshing reduced -- 7lakh --> 4lakh elements - huge reduction
;Meshing reduced. Gate and Body contact, Gpn 0.002--0.006,
;Ysti 0.353--0.3, Subs_H: 0.7, Wx NMOS and PMOS increased
;CMOS Inverter  - Active mesh reduced to reduce meshing around channel
;Shorter substrate 1--> 0.8
;Reduced STI on either sides Xsti and well: 0.1 --> 0.075
;Xmax : Ltot/3.9 --> 4.7
;Tox meshing minimum value reduced from Tox/6 --> Tox/4
;Separate drain contacts for nmos and pmos, separate for gates too
; Separate mesh for S/D regions and channel

; Add STI in the end 

(sde:clear)

(if (string=? "nMOS" "nMOS") 
 (begin
  (define DopSub "BoronActiveConcentration")
  (define DopSD  "ArsenicActiveConcentration")
;   - Substrate doping level
  (define SubDop  2e17 )   ; [1/cm3]
  (define HaloDop 2e18 ) ; [1/cm3] originally 1.5e18
 )
 (begin 
  (define DopSub "ArsenicActiveConcentration")
  (define DopSD  "BoronActiveConcentration")
;   - Substrate doping level
  (define SubDop  2e17 )   ; [1/cm3]
  (define HaloDop 2e18 ) ; [1/cm3]
 )
)
;----------------------------------------------------------------------
; Setting parameters
; - lateral
(define Lg    0.04)               ; [um] Gate length
(define Lsp   0.04)                   ; [um] Spacer length
(define Lreox 0.006)                 ; [um] Reox Spacer length
(define Ltot (+ Lg (* 2 Lsp) 0.6) )  ; [um] Lateral extend total 
(define Wx_nmos 0.12)   ; [um] Width of the NMOS device - 4 lambda= drawn gate length, where lambda = 0.03
(define Wx_pmos 0.24)   ; [um] Width of the PMOS device - 8 lambda

(define W_drain_contact 0.06) ;drain contact width
(define Y_drain_contact 0.1) ;height of the contact plug- tungsten
 
(define Xsti_btn_n_p 0.27)  ; [um] STI between NMOS and PMOS
(define Z_body 0.1)

; - layers
(define Hsub 1)   ; [um] Substrate thickness
(define Tox  18e-4) ; [um] Gate oxide thickness oxynitride
(define Hpol 0.07)   ; [um] Poly gate thickness

; - other
;   - spacer rounding
(sde:define-parameter "fillet-radius" 0.03 0.0 0.0 ) ; [um] Rounding radius

;   - pn junction resolution
(define Gpn 0.007) ; [um]

(define XjHalo 0.015)  ; [um] Halo depth 0.07 --> 0.04
(define XjExt  0.025) ; [um] Extension depth
(define XjSD   0.04)  ; [um] SD Junction depth
(define Ysilicide  -0.015)  ; [um] Silicide thickness
(define Xsti 0.07)  ; [um] STI thickness
(define Ysti 0.3)  ; [um] STI thickness
(define Xsti_minus -0.075)  ; [um] STI thickness
(define Xsti2 0)  ; [um] STI2 thickness
(define Xwell 0.07)  ; [um] well contact thickness
(define Z_vss_start  -0.07  )   ; [um]  
;----------------------------------------------------------------------
; Derived quantities
(define Xmax (/ Ltot 4.7))
(define Xmax_minus (* -1.0 Xmax))

(define Xg   (/ Lg   2.0))
(define Xg_minus (* -1.0  Xg))
(define Xsp  (+ Xg   Lsp))
(define Xsp_minus  (* -1.0  Xsp))
(define Xrox (+ Xg   Lreox))
(define Xrox_minus (* -1.0  Xrox))

(define Ysub Hsub)  ;this means Ysub=Hsub
(define Ygox (* Tox -1.0))
(define Ypol (- Ygox Hpol))

(define Xmax_sti (+ Xmax Xsti)) 
(define Xmax_sti_minus (* -1.0  Xmax_sti)) 
(define Lcont (- Xmax_sti Xsp))  


(define Xmax_well (+ Xmax_sti Xsti2 Xwell)) 
(define displacement 0 )


(define Subs_start (* Xmax_sti_minus 1.42))
(define Subs_end (* Xmax_well 1.23))
(define W_subs (+ Wx_nmos  Wx_pmos Xsti_btn_n_p 0.115) )   ; [um] Width of the substrate

(define Nwell_start (* Xmax_sti_minus 1.32))
(define Nwell_end (* Xmax_well 1.12))


(define Nwell_depth 0.6 ) ; Nwell depth


;----------------------------------------------------------------------

;----------------------------------------------------------------------
; Overlap resolution: New replaces Old
(sdegeo:set-default-boolean "ABA")

;---------------------------Device 1- NMOS-------------------------------------------
; Creating substrate region
(sdegeo:create-cuboid 
  (position    Subs_start   Ysub  0 ) 
  (position    Subs_end   0.0   W_subs ) "Silicon" "R.Substrate" )

; Substrate
(sdedr:define-constant-profile "Const.Substrate"  DopSub SubDop )
(sdedr:define-constant-profile-region  "PlaceCD.Substrate"  "Const.Substrate" "R.Substrate" )



;Active area definition- for NMOS
;(sdegeo:create-cuboid   (position    Xmax_minus  Ysub  0.0 )   (position    Xmax   0.0    Wx_nmos ) "Silicon" "R.Active1" )



;Body contact
(sdegeo:create-cuboid   (position    Xmax_sti  Ysub  0.0 )   (position    Xmax_well   0.0    Z_body ) "Silicon" "R.Body" )



; Creating gate oxide
(sdegeo:create-cuboid   (position    Xsp_minus 0.0  0.0 )   (position    Xsp Ygox Wx_nmos )   "Oxynitride" "R.Gateox"
)

; Creating PolyReox
(sdegeo:create-cuboid   (position    Xsp_minus  Ygox 0.0 )   (position    Xsp  Ypol Wx_nmos )   "Oxide" "R.PolyReox"
)

; Creating spacers regions
(sdegeo:create-cuboid   (position   (+ Xg Lreox)  Ygox 0.0 )   (position    Xsp  Ypol Wx_nmos )   "Si3N4" "R.Spacer1left"
)

; Creating spacers regions
(sdegeo:create-cuboid   (position   (- Xg_minus Lreox)  Ygox 0.0 )  (position    Xsp_minus  Ypol Wx_nmos )   "Si3N4" "R.Spacer1right")



;Poly gate
(sdegeo:create-cuboid   (position   Xg_minus Ygox 0.0 )   (position    Xg  Ypol Wx_nmos )   "PolySi" "R.Polygate")

; Creating silicide for drain
(sdegeo:create-cuboid   (position Xsp 0 0.0 )    (position Xmax Ysilicide Wx_nmos )   "NickelSilicide" "Silicide_drain" )

; Creating silicide for source
(sdegeo:create-cuboid   (position Xsp_minus 0 0.0 )    (position Xmax_minus Ysilicide Wx_nmos)   "NickelSilicide" "Silicide_source" )

;Silicide for gate
(sdegeo:create-cuboid   (position Xg_minus Ypol 0.0 )    (position Xg (+ Ypol Ysilicide) Wx_nmos )   "NickelSilicide" "Silicide_gate" )

;STI

;(sdegeo:create-cuboid (position Xmax_minus 0.0 0.0 )  (position Xmax_sti_minus Ysti Wx_nmos ) "SiO2" "STI2" )
;(sdegeo:create-cuboid (position (+ Xmax_sti Xwell) 0.0 0.0 )  (position Xmax_well Ysti Wx_nmos ) "SiO2" "STI3" )

;Field oxide or STI
;(sdegeo:create-cuboid (position  Subs_start  Ysti  0.0 )  (position Subs_end 0 W_subs) "SiO2" "Main_STI" )


;Define channel
(sdegeo:create-cuboid   (position    Xg_minus  0.0  0.0 )   (position    Xg 0.008  Wx_nmos ) "Silicon" "Channel" )





;----------------------------------------------------------------------
; - rounding spacer; fillet radius 0.03
;Select Edge- Arrow- Select the left edge of the spacer to be rounded ->Edit-3D Edit tools->Fillet

(sdegeo:fillet (list (car (find-edge-id (position -0.06 -0.0718 0.05))) (car (find-edge-id (position 0.06 -0.0718 0.05)))) 0.03)


;----------------------------------------------------------------------
--------------------------------------

; Contact declarations
;;Device 1

(sdegeo:define-contact-set "substrate"
  4.0  (color:rgb 0.0 1.0 1.0 ) "##")

(sdegeo:define-contact-set "source1" 
  4.0  (color:rgb 1.0 0.0 0.0 ) "##")

(sdegeo:define-contact-set "gate1" 
  4.0  (color:rgb 1.0 0.0 1.0 ) "##")

(sdegeo:define-contact-set "drain1" 
  4.0  (color:rgb 0.0 0.0 0.0 ) "##")


; Contact settings
(sdegeo:set-current-contact-set "substrate")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.258191489361702 0 0.06)))) "substrate")

(sdegeo:set-current-contact-set "gate1")
(sdegeo:define-3d-contact (list (car (find-face-id (position -1.73472347597681e-18 -0.0868 0.06)))) "gate1")

(sdegeo:set-current-contact-set "source1")
(sdegeo:define-3d-contact (list (car (find-face-id (position -0.106595744680851 -0.015 0.06)))) "source1")

(sdegeo:set-current-contact-set "drain1")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.106595744680851 -0.015 0.06)))) "drain1")

;--------------------------------------------------------
;;Doping Profiles:
; Device 1

;Active
;(sdedr:define-constant-profile "Const.Active"  DopSub SubDop )
;(sdedr:define-constant-profile-region  "PlaceCD.Active"  "Const.Active" "R.Active1" )


;Body1
(sdedr:define-constant-profile "Const.Body"  DopSub SubDop )
(sdedr:define-constant-profile-region  "PlaceCD.Body"  "Const.Body" "R.Body" )


; - Poly
(sdedr:define-constant-profile "Const.Gate"  DopSD 3e20 )(sdedr:define-constant-profile-region "PlaceCD.Gate"  "Const.Gate" "R.Polygate" )


;--Channel implant
(sdedr:define-refeval-window "RefEvalChannel" "Cuboid"  (position -0.017 0.01 0) (position 0.017 0 Wx_nmos))
(sdedr:define-analytical-profile-placement "Impl.Channel" "Impl.chprof" "RefEvalChannel" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.chprof" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 4e+18 "ValueAtDepth" 1e17 "Depth" 0.016 "Gauss"  "Factor" 1.0)


; - Halo 
; drain halo
(sdedr:define-refinement-window "RefEvalWin_halo" "Cuboid"   (position    0.0144   0.02 0.0)   (position 0.03 0.04 Wx_nmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo" "Impl.Haloprof2" "RefEvalWin_halo" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)


; - Halo 
; source halo
(sdedr:define-refinement-window "RefEvalWin_halo2" "Cuboid"   (position    -0.0144   0.02 0.0)   (position -0.03 0.04 Wx_nmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo2" "Impl.Haloprof2" "RefEvalWin_halo2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  drain
; -- base line definitions drain side
(sdedr:define-refinement-window "BaseLine.Ext" "Rectangle"   (position  (+ Xrox 0.002)    0.0 0.0)   (position (* Xmax 1.0) 0.0 Wx_nmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext"  "Impl.Extprof" "BaseLine.Ext" "Positive" "NoReplace" "Eval")
;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  source
; -- base line definitions source side
(sdedr:define-refinement-window "BaseLine.Ext2" "Rectangle"   (position  (- Xrox_minus 0.002)    0.0 0.0)   (position (* Xmax -1.0) 0.0 Wx_nmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext2"  "Impl.Extprof2" "BaseLine.Ext2" "Negative" "NoReplace" "Eval")

;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof2"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; Drain implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD" "Rectangle"   (position (+ Xsp 0.01) 0.0 0.0)   (position (* Xmax 1.0) 0.0 Wx_nmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD"  "Impl.SDprof" "BaseLine.SD" "Positive" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



; Source implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD2" "Rectangle"   (position (- Xsp_minus 0.01) 0.0 0.0)   (position (* Xmax -1.0) 0.0 Wx_nmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD2"  "Impl.SDprof2" "BaseLine.SD2" "Negative" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof2" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



;Deep p well and retrograde substrate doping
(sdedr:define-refeval-window "Ref_Deep_p_well" "Rectangle"  (position Xmax_sti_minus 0.5 0) (position Xmax_well 0.5 Wx_nmos))
(sdedr:define-analytical-profile-placement "Impl.Deep_p_well" "Impl.deep_p_well" "Ref_Deep_p_well" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.deep_p_well" "BoronActiveConcentration" "PeakPos" 0.7  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.9 "Gauss"  "Factor" 0.5)



;Body contact doping
(sdedr:define-refeval-window "Ref_Body_Contact" "Rectangle"  (position Xmax_sti 0 0) (position  Xmax_well  0 Z_body))
(sdedr:define-analytical-profile-placement "Impl.Body_Contact" "Impl.Body_Contact" "Ref_Body_Contact" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Body_Contact" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 5e+18 "ValueAtDepth" 1e18 "Depth" 0.02 "Gauss"  "Factor" 0.2)


;-------------------MESHING---------------------------------------------------
; Meshing Strategy: Device 1
; Substrate
;Changed this
(sdedr:define-refinement-size "Ref.Substrate" 
  (* Xmax_sti 2)  (/ Hsub 1)  (* Xmax_sti 3)   
(* Xmax_sti 1.0)  (/ Hsub 2.0)  (* Xmax_sti 2 ))
(sdedr:define-refinement-function "Ref.Substrate" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-region "RefPlace.Substrate" 
 "Ref.Substrate" "R.Substrate" )

; Active 1
(sdedr:define-refinement-window "RWin.Act" 
 "Cuboid"  
 (position  (+ Xmax_minus displacement)  0.0   0.0) 
 (position  (+ (* Xg  -1.2) displacement)  (* XjSD  1.5)    Wx_nmos) )  **1st active till the channel multibox

(sdedr:define-refinement-size "Ref.SiAct" 
  (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct" 
 "Ref.SiAct" "RWin.Act" )



; Active 2
(sdedr:define-refinement-window "RWin.Act_part2" 
 "Cuboid"  
 (position  (+ Xmax displacement)  0.0   0.0) 
 (position  (+ (* Xg  1.2) displacement)  (* XjSD  1.5)    Wx_nmos) )  **2nd active till the channel multibox

(sdedr:define-refinement-size "Ref.SiAct_part2" 
  (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct_part2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct_part2" 
 "Ref.SiAct_part2" "RWin.Act_part2" )





; Poly Gate Multibox
(sdedr:define-refinement-window "MBWindow.Gate" 
 "Cuboid"
 (position Xg_minus  Ypol   0.0) 
 (position Xg  Ygox  Wx_nmos) )
;Changed this
(sdedr:define-multibox-size "MBSize.Gate" 
  (/ Lg  2.0)  (/ Hpol 2.0)  (/ Lg  2.0) 
  (/ Lg 3.0)   (/ Hpol 4.0)   (/ Lg 3.0)
  1.0         -1.35   1.0)
(sdedr:define-multibox-placement "MBPlace.Gate" 
 "MBSize.Gate"  "MBWindow.Gate" )


; GateOx
(sdedr:define-refinement-size "Ref.GOX" 
 (/ Xmax_well 1.5)  (/ Tox 2.0)    (/ Xmax_well 1.5)
  Gpn           (/ Tox 3.0)  Gpn  )
(sdedr:define-refinement-region "RefPlace.GOX" 
 "Ref.GOX" "R.Gateox" )

; Channel Multibox
(sdedr:define-refinement-window "MBWindow.Channel" 
 "Cuboid"  
 (position  (* Xg  -1.2)  0.0   0.0) 
 (position (* Xg  1.2) (* 2.0 XjExt)  Wx_nmos) )
(sdedr:define-multibox-size "MBSize.Channel" 
  (/ Lg  3.0)  (/ XjSD 4.0)  (/ Lg  3.0)
 (/ Lg 6.0)  1e-3   (/ Lg 5.0)
  1.0     1.35  1.0)
(sdedr:define-multibox-placement "MBPlace.Channel" 
 "MBSize.Channel" "MBWindow.Channel" )



; Body contact
(sdedr:define-refinement-window "RWin.Body" 
  "Cuboid"
 (position   Xmax_sti  0.0   0.0) 
 (position  (+ Xmax_sti Xwell) (/ Ysti 4)   Z_body) )

(sdedr:define-refinement-size "Ref.Body" 
  (/ Xmax 1.3) (/  Xmax 1.3) (/ Xmax 1.3)
 (/ Xmax 2.0) (/ Xmax 2.0) (/ Xmax 2.0))
(sdedr:define-refinement-function "Ref.Body" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.Body" 
 "Ref.Body" "RWin.Body" )


;-------------------------Device 2- PMOS------------------------

; Define the start and end Z co-ordinates of pmos
(define Zstart_pmos (+  Wx_nmos Xsti_btn_n_p))
(define Zend_pmos (+  Wx_nmos Xsti_btn_n_p Wx_pmos))


(define Z_nwell_start (- Zstart_pmos (/ Xsti_btn_n_p  3.9) ) )
(define Z_nwell_end (+ Zend_pmos (/ Xsti_btn_n_p  3.9) ) )




;Nwell - for PMOS
(sdegeo:create-cuboid   (position  Nwell_start   Nwell_depth  Z_nwell_start )   (position    Nwell_end   0.0   Z_nwell_end) "Silicon" "R.NWell_Si" )


;Body contact
(sdegeo:create-cuboid   (position    Xmax_sti  Ysub  Zstart_pmos)   (position    Xmax_well   0.0    (+ Zstart_pmos 0.1) ) "Silicon" "R.Body_pmos" )


; Creating gate oxide
(sdegeo:create-cuboid 
  (position    Xsp_minus 0.0   Zstart_pmos )   (position    Xsp Ygox    Zend_pmos )   "Oxynitride" "R.Gateox_pmos")

; Creating PolyReox
(sdegeo:create-cuboid 
  (position    Xsp_minus  Ygox  Zstart_pmos)   (position    Xsp  Ypol    Zend_pmos )   "Oxide" "R.PolyReox_pmos")

; Creating spacers regions
(sdegeo:create-cuboid 
  (position   (+ Xg Lreox)  Ygox  Zstart_pmos)  (position    Xsp  Ypol    Zend_pmos)   "Si3N4" "R.Spacer1left_pmos")

; Creating spacers regions
(sdegeo:create-cuboid   (position   (- Xg_minus Lreox)  Ygox  Zstart_pmos )   (position    Xsp_minus  Ypol    Zend_pmos )   "Si3N4" "R.Spacer1right_pmos"
)


;Poly gate
(sdegeo:create-cuboid   (position   Xg_minus Ygox  Zstart_pmos )   (position    Xg  Ypol  Zend_pmos )   "PolySi" "R.Polygate_pmos")



; Creating silicide for drain
(sdegeo:create-cuboid   (position Xsp 0  Zstart_pmos )    (position Xmax Ysilicide Zend_pmos )   "NickelSilicide" "Silicide_drain_pmos" )

; Creating silicide for source
(sdegeo:create-cuboid   (position Xsp_minus 0  Zstart_pmos )    (position Xmax_minus Ysilicide Zend_pmos)   "NickelSilicide" "Silicide_source_pmos" )

;Silicide for gate
(sdegeo:create-cuboid   (position Xg_minus Ypol  Zstart_pmos )    (position Xg (+ Ypol Ysilicide) Zend_pmos )   "NickelSilicide" "Silicide_gate_pmos" )

;Define channel
(sdegeo:create-cuboid   (position    Xg_minus  0.0   Zstart_pmos )   (position    Xg 0.008  Zend_pmos ) "Silicon" "Channel_pmos" )

;----------------------------------------------------------------------
; - rounding spacer
;Select Edge- Arrow- Select the left edge of the spacer to be rounded ->Edit-3D Edit tools->Fillet

(sdegeo:fillet (list (car (find-edge-id (position -0.06 -0.0718 0.42)))) 0.03)

(sdegeo:fillet (list (car (find-edge-id (position  0.06 -0.0718 0.42)))) 0.03)
;-----------------------------------------

;---


;Device pmos

(sdegeo:define-contact-set "source2" 
  4.0  (color:rgb 1.0 1.0 1.0 ) "##")


(sdegeo:define-contact-set "nwell_pmos" 
  4.0  (color:rgb 0.0 1.0 1.0 ) "==")


(sdegeo:define-contact-set "gate2" 
  4.0  (color:rgb 0.0 1.0 0.0 ) "==")

(sdegeo:define-contact-set "drain2" 
  4.0  (color:rgb 0.0 0.0 0.0 ) "==")



;----------------------------------------------------------------------
;Defining contacts for both the devices
(sdegeo:set-current-contact-set "source2")
(sdegeo:define-3d-contact (list (car (find-face-id (position -0.106595744680851 -0.015 0.54)))) "source2")

(sdegeo:set-current-contact-set "gate2")
(sdegeo:define-3d-contact (list (car (find-face-id (position -1.73472347597681e-18 -0.0868 0.54)))) "gate2")

(sdegeo:set-current-contact-set "drain2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.106595744680851 -0.015 0.54)))) "drain2")

(sdegeo:set-current-contact-set "nwell_pmos")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.258191489361702 0 0.47)))) "nwell_pmos")
;----------------------------------------------------------------------
;----------------------------------------------------------------------
;Rename the regions of the translated device PMOS - to make it easier for doping and meshing.
;Edit->Change Region Name

;------------Doping Profiles:---------------------------------------------------------

(define DopSub_pmos "ArsenicActiveConcentration")
(define DopSD_pmos  "BoronActiveConcentration")
; Substrate doping level
(define SubDop_pmos  2e17 )   ; [1/cm3]
(define HaloDop_pmos 2e18 ) ; [1/cm3]




; Device 2



;Nwell for PMOS- light p-doping first
(sdedr:define-constant-profile "Const.NWell_Si" "BoronActiveConcentration" 1e15)
(sdedr:define-constant-profile-region "PlaceCD.NWell_Si"  "Const.NWell_Si" "R.NWell_Si" )



;Nwell 
(sdedr:define-refeval-window "RefEvalNwell" "Cuboid"  (position  Nwell_start   Nwell_depth  Z_nwell_start )   (position    Nwell_end   0.0   Z_nwell_end) )
(sdedr:define-constant-profile "Const.Nwell" "ArsenicActiveConcentration" 2e+17)
(sdedr:define-constant-profile-placement "PlaceCD.Nwell" "Const.Nwell" "RefEvalNwell")




;Active 2 for PMOS- light p-doping
;(sdedr:define-constant-profile "Const.Active2" "BoronActiveConcentration" 1e15)
;(sdedr:define-constant-profile-region "PlaceCD.Active2"  "Const.Active2" "R.Active2" )



;Body2 substrate doping
(sdedr:define-constant-profile "Const.Body_pmos"  "BoronActiveConcentration" 1e15 )
(sdedr:define-constant-profile-region  "PlaceCD.Body_pmos"  "Const.Body_pmos" "R.Body_pmos" )



;Body1 substrate doping
(sdedr:define-constant-profile "Const.Body_pmosN"  "ArsenicActiveConcentration" 2e+17 )
(sdedr:define-constant-profile-region  "PlaceCD.Body_pmosN"  "Const.Body_pmosN" "R.Body_pmos" )



; - Poly
(sdedr:define-constant-profile "Const.Gate2"  DopSD_pmos 3e20 )(sdedr:define-constant-profile-region "PlaceCD.Gate2"  "Const.Gate2" "R.Polygate_pmos" )




;--Channel implant
(sdedr:define-refeval-window "RefEvalChannel_pmos" "Cuboid"  (position -0.017 0 Zstart_pmos) (position 0.017 0.01  Zend_pmos));
(sdedr:define-analytical-profile-placement "Impl.Channel_pmos" "Impl.ch_pmos" "RefEvalChannel_pmos" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.ch_pmos" DopSub_pmos "PeakPos" 0  "PeakVal" 4e+18 "ValueAtDepth" 1e17 "Depth" 0.016 "Gauss"  "Factor" 1.0)


; - Halo 
; drain halo
(sdedr:define-refinement-window "RefEvalWin_halo_pmos" "Cuboid"   (position    0.0144   0.02 Zstart_pmos)   (position 0.03 0.04 Zend_pmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo_pmos" "Impl.Haloprof2_pmos" "RefEvalWin_halo_pmos" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2_pmos" DopSub_pmos "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)


; - Halo 
; source halo
(sdedr:define-refinement-window "RefEvalWin_halo2_pmos" "Cuboid"   (position    -0.0144   0.02 Zstart_pmos)   (position -0.03 0.04 Zend_pmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo2_pmos" "Impl.Haloprof2_pmos" "RefEvalWin_halo2_pmos" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2_pmos" DopSub_pmos "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  drain
; -- base line definitions drain side
(sdedr:define-refinement-window "BaseLine.Ext_pmos" "Rectangle"   (position  (+ Xrox 0.002)    0.0  Zstart_pmos)   (position (* Xmax 1.0) 0.0 Zend_pmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext_pmos"  "Impl.Extprof_pmos" "BaseLine.Ext_pmos" "Positive" "NoReplace" "Eval")
;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof_pmos"  DopSD_pmos "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  source
; -- base line definitions source side
(sdedr:define-refinement-window "BaseLine.Ext2_pmos" "Rectangle"   (position  (- Xrox_minus 0.002)    0.0   Zstart_pmos)   (position (* Xmax -1.0) 0.0 Zend_pmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext2_pmos"  "Impl.Extprof2_pmos" "BaseLine.Ext2_pmos" "Negative" "NoReplace" "Eval")

;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof2_pmos"  DopSD_pmos "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; Drain implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD_pmos" "Rectangle"   (position (+ Xsp 0.01) 0.0  Zstart_pmos)   (position (* Xmax 1.0) 0.0 Zend_pmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD_pmos"  "Impl.SDprof_pmos" "BaseLine.SD_pmos" "Positive" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof_pmos" DopSD_pmos  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



; Source implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD2_pmos" "Rectangle"   (position (- Xsp_minus 0.01) 0.0  Zstart_pmos)   (position (* Xmax -1.0) 0.0 Zend_pmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD2_pmos"  "Impl.SDprof2_pmos" "BaseLine.SD2_pmos" "Negative" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof2_pmos" DopSD_pmos  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)


;Body contact doping
(sdedr:define-refeval-window "Ref_Body_Contact_pmos" "Rectangle"  (position Xmax_sti 0  Zstart_pmos) (position  Xmax_well  0 (+ Zstart_pmos 0.1)))
(sdedr:define-analytical-profile-placement "Impl.Body_Contact_pmos" "Impl.Body_Contact_pmos" "Ref_Body_Contact_pmos" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Body_Contact_pmos" DopSub_pmos "PeakPos" 0  "PeakVal" 5e+18 "ValueAtDepth" 1e18 "Depth" 0.02 "Gauss"  "Factor" 0.2)


;-------------------MESHING---------------------------------------------------
;------------------------MESHING 2----------------------------------------------

; Active 2 - PMOS

(sdedr:define-refinement-window "RWin.Act_pmos" 
 "Cuboid"  
  (position   (+ Xmax_minus displacement)  0.0   Zstart_pmos) 
 (position  (+ (* Xg  -1.2) displacement)  (* XjSD  1.5)    Zend_pmos) )

(sdedr:define-refinement-size "Ref.SiAct_pmos" 
    (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct_pmos" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct_pmos" 
 "Ref.SiAct_pmos" "RWin.Act_pmos" )




(sdedr:define-refinement-window "RWin.Act_pmos_part2" 
 "Cuboid"  
 (position   (+ Xmax displacement)  0.0   Zstart_pmos) 
 (position  (+ (* Xg  1.2) displacement)  (* XjSD  1.5)    Zend_pmos) )

(sdedr:define-refinement-size "Ref.SiAct_pmos_part2" 
    (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct_pmos_part2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct_pmos_part2" 
 "Ref.SiAct_pmos_part2" "RWin.Act_pmos_part2" )




; Poly Gate Multibox
(sdedr:define-refinement-window "MBWindow.Gate_pmos" 
 "Cuboid"
 (position Xg_minus Ypol   Zstart_pmos) 
 (position Xg  Ygox  Zend_pmos) )

(sdedr:define-multibox-size "MBSize.Gate_pmos" 
  (/ Lg  2.0)  (/ Hpol 2.0)  (/ Lg  2.0) 
  (/ Lg 3.0)   (/ Hpol 4.0)   (/ Lg 3.0)
  1.0         -1.35   1.0)
(sdedr:define-multibox-placement "MBPlace.Gate_pmos" 
 "MBSize.Gate_pmos"  "MBWindow.Gate_pmos" )


; GateOx
(sdedr:define-refinement-size "Ref.GOX_pmos" 
 (/ Xmax_well 1.5)  (/ Tox 2.0)    (/ Xmax_well 1.5)
  Gpn           (/ Tox 3.0)  Gpn  )
(sdedr:define-refinement-region "RefPlace.GOX_pmos" 
 "Ref.GOX_pmos" "R.Gateox_pmos" )

; Channel Multibox
(sdedr:define-refinement-window "MBWindow.Channel_pmos" 
 "Cuboid"  
 (position  (* Xg  -1.2)  0.0   Zstart_pmos) 
 (position (* Xg  1.2) (* 2.0 XjExt)  Zend_pmos) )
(sdedr:define-multibox-size "MBSize.Channel_pmos" 
  (/ Lg  3.0)  (/ XjSD 4.0)  (/ Lg  3.0)
 (/ Lg 6.0)  1e-3   (/ Lg 5.0)
  1.0     1.35  1.0)
(sdedr:define-multibox-placement "MBPlace.Channel_pmos" 
 "MBSize.Channel_pmos" "MBWindow.Channel_pmos" )



; Body contact
(sdedr:define-refinement-window "RWin.Body_pmos" 
  "Cuboid"
 (position   Xmax_sti  0.0   Zstart_pmos) 
 (position  (+ Xmax_sti Xwell) (/ Ysti 4)   (+  Zstart_pmos 0.1)) )

(sdedr:define-refinement-size "Ref.Body_pmos" 
  (/ Xmax 1.3) (/  Xmax 1.3) (/ Xmax 1.3)
 (/ Xmax 2.0) (/ Xmax 2.0) (/ Xmax 2.0))
(sdedr:define-refinement-function "Ref.Body_pmos" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.Body_pmos" 
 "Ref.Body_pmos" "RWin.Body_pmos" )



;---------------------------------------------------------------------------------------_
;--------------------------------------Inverter 2----------------------------------------------
;---------------------------------------------------------------------------------------_
;NMOS
(define DopSub "BoronActiveConcentration")
 define DopSD  "ArsenicActiveConcentration")
;   - Substrate doping level
  (define SubDop  2e17 )   ; [1/cm3]
  (define HaloDop 2e18 ) ; [1/cm3] originally 1.5e18


(define displacement (- Subs_end Subs_start))

;---------------------------Inv 2- NMOS-------------------------------------------
; Creating substrate region
(sdegeo:create-cuboid 
  (position    (+ Subs_start displacement)   Ysub  0 ) 
  (position    (+ Subs_end  displacement)  0.0   W_subs ) "Silicon" "R.Substrate_i2" )

; Substrate
(sdedr:define-constant-profile "Const.Substrate_i2"  DopSub SubDop )
(sdedr:define-constant-profile-region  "PlaceCD.Substrate_i2"  "Const.Substrate_i2" "R.Substrate_i2" )



;Body contact
(sdegeo:create-cuboid   (position  (- (+ Xmax_minus displacement) Xsti)  Ysub  0.0 )   (position   (- (+ Xmax_minus displacement) (* 2 Xsti))    0.0   Z_body ) "Silicon" "R.Body_i2" )



; Creating gate oxide
(sdegeo:create-cuboid   (position  (+  Xsp_minus displacement) 0.0  0.0 )   (position   (+ Xsp displacement) Ygox Wx_nmos )   "Oxynitride" "R.Gateox_i2"
)

; Creating PolyReox
(sdegeo:create-cuboid   (position   (+ Xsp_minus displacement) Ygox 0.0 )   (position   (+ Xsp displacement)  Ypol Wx_nmos )   "Oxide" "R.PolyReox_i2"
)

; Creating spacers regions
(sdegeo:create-cuboid   (position   (+ Xg Lreox  displacement)  Ygox 0.0 )   (position  (+  Xsp displacement)  Ypol Wx_nmos )   "Si3N4" "R.Spacer1left_i2"
)

; Creating spacers regions
(sdegeo:create-cuboid   (position   (+ (- Xg_minus Lreox) displacement) Ygox 0.0 )  (position  (+  Xsp_minus displacement)  Ypol Wx_nmos )   "Si3N4" "R.Spacer1right_i2")



;Poly gate
(sdegeo:create-cuboid   (position  (+ Xg_minus displacement) Ygox 0.0 )   (position   (+ Xg displacement) Ypol Wx_nmos )   "PolySi" "R.Polygate_i2")

; Creating silicide for drain
(sdegeo:create-cuboid   (position (+ Xsp displacement) 0 0.0 )    (position (+ Xmax displacement) Ysilicide Wx_nmos )   "NickelSilicide" "Silicide_drain_i2" )

; Creating silicide for source
(sdegeo:create-cuboid   (position (+ Xsp_minus displacement) 0 0.0 )    (position (+ Xmax_minus displacement) Ysilicide Wx_nmos)   "NickelSilicide" "Silicide_source_i2" )

;Silicide for gate
(sdegeo:create-cuboid   (position (+ Xg_minus displacement) Ypol 0.0 )    (position (+ Xg displacement) (+ Ypol Ysilicide) Wx_nmos )   "NickelSilicide" "Silicide_gate_i2" )


;Define channel
(sdegeo:create-cuboid   (position   (+ Xg_minus displacement) 0.0  0.0 )   (position   (+ Xg displacement) 0.008  Wx_nmos ) "Silicon" "Channel_i2" )





;----------------------------------------------------------------------
; - rounding spacer; fillet radius 0.03
;Select Edge- Arrow- Select the left edge of the spacer to be rounded ->Edit-3D Edit tools->Fillet

(sdegeo:fillet (list (car (find-edge-id (position (+ -0.06 displacement) -0.0718 0.05))) (car (find-edge-id (position (+ 0.06 displacement) -0.0718 0.05)))) 0.03)

;----------------------------------------------------------------------
--------------------------------------

; Contact declarations
;;Device 1

(sdegeo:define-contact-set "substrate_i2"
  4.0  (color:rgb 0.0 1.0 1.0 ) "||")

(sdegeo:define-contact-set "source1_i2" 
  4.0  (color:rgb 0.0 1.0 0.0 ) "||")

(sdegeo:define-contact-set "gate1_i2" 
  4.0  (color:rgb 0.0 0.0 1.0 ) "||")

(sdegeo:define-contact-set "drain1_i2" 
  4.0  (color:rgb 0.0 0.0 0.0 ) "||")


; Contact settings

;(sdegeo:define-3d-contact (list (car (find-face-id (position (+ 0.265691489361702 displacement) 0 0.03)))) "substrate_i2")

(sdegeo:set-current-contact-set "gate1_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.673093617021277 -0.0868 0.06)))) "gate1_i2")


(sdegeo:set-current-contact-set "drain1_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.566497872340425 -0.015 0.06)))) "drain1_i2")

(sdegeo:set-current-contact-set "source1_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.779689361702128 -0.015 0.06)))) "source1_i2")

(sdegeo:set-current-contact-set "substrate_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.419365957446808 0 0.05)))) "substrate_i2")

; -----------------------------------------------------------------------------
;;Doping Profiles:
; Device 1

;Body1
(sdedr:define-constant-profile "Const.Body_i2"  DopSub SubDop )
(sdedr:define-constant-profile-region  "PlaceCD.Body_i2"  "Const.Body_i2" "R.Body_i2" )


; - Poly
(sdedr:define-constant-profile "Const.Gate_i2"  DopSD 3e20 )(sdedr:define-constant-profile-region "PlaceCD.Gate_i2"  "Const.Gate_i2" "R.Polygate_i2" )


;--Channel implant
(sdedr:define-refeval-window "RefEvalChannel_i2" "Cuboid"  (position (+ -0.017 displacement) 0.01 0) (position (+ 0.017 displacement) 0 Wx_nmos))
(sdedr:define-analytical-profile-placement "Impl.Channel_i2" "Impl.chprof_i2" "RefEvalChannel_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.chprof_i2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 4e+18 "ValueAtDepth" 1e17 "Depth" 0.016 "Gauss"  "Factor" 1.0)


; - Halo 
; drain halo
(sdedr:define-refinement-window "RefEvalWin_halo_i2" "Cuboid"   (position   (+ 0.0144 displacement)  0.02 0.0)   (position (+ 0.03 displacement) 0.04 Wx_nmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo_i2" "Impl.Haloprof2_i2" "RefEvalWin_halo_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2_i2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)


; - Halo 
; source halo
(sdedr:define-refinement-window "RefEvalWin_halo2_i2" "Cuboid"   (position   (+ -0.0144 displacement)  0.02 0.0)   (position  (+ -0.03 displacement) 0.04 Wx_nmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo2_i2" "Impl.Haloprof2_i2" "RefEvalWin_halo2_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2_i2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  drain
; -- base line definitions drain side
(sdedr:define-refinement-window "BaseLine.Ext_i2" "Rectangle"   (position  (+ Xrox 0.002 displacement)    0.0 0.0)   (position (+ (* Xmax 1.0) displacement) 0.0 Wx_nmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext_i2"  "Impl.Extprof_i2" "BaseLine.Ext_i2" "Positive" "NoReplace" "Eval")
;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof_i2"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  source
; -- base line definitions source side
(sdedr:define-refinement-window "BaseLine.Ext2_i2" "Rectangle"   (position  (+ (- Xrox_minus 0.002) displacement)   0.0 0.0)   (position (+ (* Xmax -1.0) displacement) 0.0 Wx_nmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext2_i2"  "Impl.Extprof2_i2" "BaseLine.Ext2_i2" "Negative" "NoReplace" "Eval")

;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof2_i2"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt "Gauss"  "Factor" 0.6)



; Drain implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD_i2" "Rectangle"   (position (+ Xsp 0.01 displacement) 0.0 0.0)   (position (+ (* Xmax 1.0) displacement) 0.0 Wx_nmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD_i2"  "Impl.SDprof_i2" "BaseLine.SD_i2" "Positive" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof_i2" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



; Source implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD2_i2" "Rectangle"   (position (+ (- Xsp_minus 0.01) displacement) 0.0 0.0)   (position (+ (* Xmax -1.0) displacement) 0.0 Wx_nmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD2_i2"  "Impl.SDprof2_i2" "BaseLine.SD2_i2" "Negative" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof2_i2" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



;Deep p well and retrograde substrate doping
(sdedr:define-refeval-window "Ref_Deep_p_well_i2" "Rectangle"  (position (+ Xmax_sti_minus displacement) 0.5 0) (position (+ Xmax_well displacement) 0.5 Wx_nmos))
(sdedr:define-analytical-profile-placement "Impl.Deep_p_well_i2" "Impl.deep_p_well_i2" "Ref_Deep_p_well_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.deep_p_well_i2" "BoronActiveConcentration" "PeakPos" 0.7  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.9 "Gauss"  "Factor" 0.5)



;Body contact doping
(sdedr:define-refeval-window "Ref_Body_Contact_i2" "Rectangle"  (position (- (+ Xmax_minus displacement) Xsti)  0 0) (position  (- (+ Xmax_minus displacement) (* 2 Xsti))  0  Z_body))
(sdedr:define-analytical-profile-placement "Impl.Body_Contact_i2" "Impl.Body_Contact_i2" "Ref_Body_Contact_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Body_Contact_i2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 5e+18 "ValueAtDepth" 1e18 "Depth" 0.02 "Gauss"  "Factor" 0.2)


;-------------------MESHING---------------------------------------------------
; Meshing Strategy: Device 1 - Inv2
; Substrate
(sdedr:define-refinement-size "Ref.Substrate_i2" 
  (/ Xmax_sti 1)  (/ Hsub 2.0)  (/ Xmax_sti 1)
(/ Xmax_sti 2.0)  (/ Hsub 3.0)  (/ Xmax_sti 2.0))
(sdedr:define-refinement-function "Ref.Substrate_i2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-region "RefPlace.Substrate_i2" 
 "Ref.Substrate_i2" "R.Substrate_i2" )

; Active 1
(sdedr:define-refinement-window "RWin.Act_i2" 
 "Cuboid"  
 (position  (+ Xmax_minus displacement)  0.0   0.0) 
 (position  (+ (* Xg  -1.2) displacement)  (* XjSD  1.5)    Wx_nmos) )  **1st active till the channel multibox

(sdedr:define-refinement-size "Ref.SiAct_i2" 
  (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct_i2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct_i2" 
 "Ref.SiAct_i2" "RWin.Act_i2" )



; Active 2
(sdedr:define-refinement-window "RWin.Act_part2_i2" 
 "Cuboid"  
 (position  (+ Xmax displacement)  0.0   0.0) 
 (position  (+ (* Xg  1.2) displacement)  (* XjSD  1.5)    Wx_nmos) )  **2nd active till the channel multibox

(sdedr:define-refinement-size "Ref.SiAct_part2_i2" 
  (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct_part2_i2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct_part2_i2" 
 "Ref.SiAct_part2_i2" "RWin.Act_part2_i2" )





; Poly Gate Multibox
(sdedr:define-refinement-window "MBWindow.Gate_i2" 
 "Cuboid"
 (position (+ Xg_minus displacement) Ypol   0.0) 
 (position (+ Xg displacement) Ygox  Wx_nmos) )

(sdedr:define-multibox-size "MBSize.Gate_i2" 
  (/ Lg  2.0)  (/ Hpol 2.0)  (/ Lg  2.0) 
  (/ Lg 3.0)   (/ Hpol 4.0)   (/ Lg 3.0)
  1.0         -1.35   1.0)
(sdedr:define-multibox-placement "MBPlace.Gate_i2" 
 "MBSize.Gate_i2"  "MBWindow.Gate_i2" )


; GateOx
(sdedr:define-refinement-size "Ref.GOX_i2" 
 (/ Xmax_well 1.5)  (/ Tox 2.0)    (/ Xmax_well 1.5)
  Gpn           (/ Tox 3.0)  Gpn  )
(sdedr:define-refinement-region "RefPlace.GOX_i2" 
 "Ref.GOX_i2" "R.Gateox_i2" )

; Channel Multibox
(sdedr:define-refinement-window "MBWindow.Channel_i2" 
 "Cuboid"  
 (position  (+ (* Xg  -1.2) displacement) 0.0   0.0) 
 (position (+ (* Xg  1.2) displacement) (* 2.0 XjExt)  Wx_nmos) )
(sdedr:define-multibox-size "MBSize.Channel_i2" 
  (/ Lg  3.0)  (/ XjSD 4.0)  (/ Lg  3.0)
 (/ Lg 6.0)  1e-3   (/ Lg 5.0)
  1.0     1.35  1.0)
(sdedr:define-multibox-placement "MBPlace.Channel_i2" 
 "MBSize.Channel_i2" "MBWindow.Channel_i2" )



; Body contact
(sdedr:define-refinement-window "RWin.Body_i2" 
  "Cuboid"
 (position  (- (+ Xmax_minus displacement) Xsti)  0.0   0.0) 
 (position  (- (+ Xmax_minus displacement) (* 2 Xsti)) (/ Ysti 4)   Z_body) )

(sdedr:define-refinement-size "Ref.Body_i2" 
  (/ Xmax 1.3) (/  Xmax 1.3) (/ Xmax 1.3)
 (/ Xmax 2.0) (/ Xmax 2.0) (/ Xmax 2.0))
(sdedr:define-refinement-function "Ref.Body_i2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.Body_i2" 
 "Ref.Body_i2" "RWin.Body_i2" )


;-------------------------Device 2- PMOS------------------------

(sdegeo:create-cuboid   (position  Nwell_end   Nwell_depth  Z_nwell_start )   (position     (+ Nwell_start displacement)   0.0   Z_nwell_end) "Silicon" "R.NWell_btn" )

;Nwell - for PMOS
(sdegeo:create-cuboid   (position  (+ Nwell_start displacement)  Nwell_depth  Z_nwell_start )   (position   (+ Nwell_end displacement)  0.0   Z_nwell_end) "Silicon" "R.NWell_Si_i2" )


;Body contact
(sdegeo:create-cuboid   (position  (- (+ Xmax_minus displacement) Xsti) Ysub  Zstart_pmos)   (position   (- (+ Xmax_minus displacement) (* 2 Xsti))  0.0    (+ Zstart_pmos 0.1) ) "Silicon" "R.Body_pmos_i2" )


; Creating gate oxide
(sdegeo:create-cuboid   (position   (+ Xsp_minus displacement) 0.0   Zstart_pmos )   (position   (+  Xsp displacement) Ygox    Zend_pmos )   "Oxynitride" "R.Gateox_pmos_i2")

; Creating PolyReox
(sdegeo:create-cuboid   (position   (+ Xsp_minus displacement) Ygox  Zstart_pmos)   (position  (+ Xsp displacement) Ypol    Zend_pmos )   "Oxide" "R.PolyReox_pmos_i2")

; Creating spacers regions
(sdegeo:create-cuboid 
  (position   (+ Xg Lreox displacement)  Ygox  Zstart_pmos)  (position   (+ Xsp displacement) Ypol    Zend_pmos)   "Si3N4" "R.Spacer1left_pmos_i2")

; Creating spacers regions
(sdegeo:create-cuboid   (position  (+ (- Xg_minus Lreox) displacement) Ygox  Zstart_pmos )   (position    (+ Xsp_minus displacement) Ypol    Zend_pmos )   "Si3N4" "R.Spacer1right_pmos_i2")


;Poly gate
(sdegeo:create-cuboid   (position  (+ Xg_minus displacement) Ygox  Zstart_pmos )   (position   (+ Xg displacement) Ypol  Zend_pmos )   "PolySi" "R.Polygate_pmos_i2")



; Creating silicide for drain
(sdegeo:create-cuboid   (position (+ Xsp displacement) 0  Zstart_pmos )    (position  (+ Xmax displacement) Ysilicide Zend_pmos )   "NickelSilicide" "Silicide_drain_pmos_i2" )

; Creating silicide for source
(sdegeo:create-cuboid   (position (+ Xsp_minus displacement) 0  Zstart_pmos )    (position (+ Xmax_minus displacement) Ysilicide Zend_pmos)   "NickelSilicide" "Silicide_source_pmos_i2" )

;Silicide for gate
(sdegeo:create-cuboid   (position (+ Xg_minus displacement) Ypol  Zstart_pmos )    (position (+ Xg displacement) (+ Ypol Ysilicide) Zend_pmos )   "NickelSilicide" "Silicide_gate_pmos_i2" )

;Define channel
(sdegeo:create-cuboid   (position   (+ Xg_minus displacement) 0.0   Zstart_pmos )   (position   (+ Xg displacement) 0.008  Zend_pmos ) "Silicon" "Channel_pmos_i2" )

;----------------------------------------------------------------------
; - rounding spacer
;Select Edge- Arrow- Select the left edge of the spacer to be rounded ->Edit-3D Edit tools->Fillet

(sdegeo:fillet (list (car (find-edge-id (position (+ -0.06 displacement) -0.0718 0.42)))) 0.03)

(sdegeo:fillet (list (car (find-edge-id (position (+ 0.06 displacement) -0.0718 0.42)))) 0.03)
;-----------------------------------------


;Device pmos

(sdegeo:define-contact-set "source2_i2" 
  4.0  (color:rgb 1.0 1.0 0.0 ) "==")


(sdegeo:define-contact-set "nwell_pmos_i2" 
  4.0  (color:rgb 1.0 0.0 1.0 ) "==")


(sdegeo:define-contact-set "gate2_i2" 
  4.0  (color:rgb 1.0 0.0 0.0 ) "==")

(sdegeo:define-contact-set "drain2_i2" 
  4.0  (color:rgb 0.0 0.0 1.0 ) "==")



;----------------------------------------------------------------------
;Defining contacts for both the devices

(sdegeo:set-current-contact-set "drain2_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.566497872340425 -0.015 0.54)))) "drain2_i2")

(sdegeo:set-current-contact-set "gate2_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.673093617021277 -0.0868 0.54)))) "gate2_i2")

(sdegeo:set-current-contact-set "source2_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.779689361702128 -0.015 0.54)))) "source2_i2")

(sdegeo:set-current-contact-set "nwell_pmos_i2")
(sdegeo:define-3d-contact (list (car (find-face-id (position 0.419365957446808 0 0.44)))) "nwell_pmos_i2")


;----------------------------------------------------------------------
;----------------------------------------------------------------------
;Rename the regions of the translated device PMOS - to make it easier for doping and meshing.
;Edit->Chane Region Name

;------------Doping Profiles:---------------------------------------------------------

(define DopSub_pmos "ArsenicActiveConcentration")
(define DopSD_pmos  "BoronActiveConcentration")
; Substrate doping level
(define SubDop_pmos  2e17 )   ; [1/cm3]
(define HaloDop_pmos 2e18 ) ; [1/cm3]




; Device 2 - Inv2 pmos



;Nwell for PMOS- light p-doping first
(sdedr:define-constant-profile "Const.NWell_Si_i2" "BoronActiveConcentration" 1e15)
(sdedr:define-constant-profile-region "PlaceCD.NWell_Si_i2"  "Const.NWell_Si_i2" "R.NWell_Si_i2" )



;Nwell 
(sdedr:define-refeval-window "RefEvalNwell_i2" "Cuboid"  (position (+ Nwell_start  displacement) Nwell_depth  Z_nwell_start )   (position    (+ Nwell_end displacement)  0.0   Z_nwell_end) )
(sdedr:define-constant-profile "Const.Nwell_i2" "ArsenicActiveConcentration" 2e+17)
(sdedr:define-constant-profile-placement "PlaceCD.Nwell_i2" "Const.Nwell_i2" "RefEvalNwell_i2")


;Nwell-btn for PMOS- light p-doping first
(sdedr:define-constant-profile "Const.NWell_btn" "BoronActiveConcentration" 1e15)
(sdedr:define-constant-profile-region "PlaceCD.NWell_btn"  "Const.NWell_btn" "R.NWell_btn" )



;Nwell -btn
(sdedr:define-refeval-window "RefEvalNwell_btn" "Cuboid"  (position  Nwell_end Nwell_depth  Z_nwell_start )   (position    (+ Nwell_start displacement)  0.0   Z_nwell_end) )
(sdedr:define-constant-profile "Const.Nwell_btn" "ArsenicActiveConcentration" 2e+17)
(sdedr:define-constant-profile-placement "PlaceCD.Nwell_btn" "Const.Nwell_btn" "RefEvalNwell_btn")



;Body2 substrate doping
(sdedr:define-constant-profile "Const.Body_pmos_i2"  "BoronActiveConcentration" 1e15 )
(sdedr:define-constant-profile-region  "PlaceCD.Body_pmos_i2"  "Const.Body_pmos_i2" "R.Body_pmos_i2" )



;Body1 substrate doping
(sdedr:define-constant-profile "Const.Body_pmosN_i2"  "ArsenicActiveConcentration" 2e+17 )
(sdedr:define-constant-profile-region  "PlaceCD.Body_pmosN_i2"  "Const.Body_pmosN_i2" "R.Body_pmos_i2" )



; - Poly
(sdedr:define-constant-profile "Const.Gate2_i2"  DopSD_pmos 3e20 )(sdedr:define-constant-profile-region "PlaceCD.Gate2_i2"  "Const.Gate2_i2" "R.Polygate_pmos_i2" )




;--Channel implant
(sdedr:define-refeval-window "RefEvalChannel_pmos_i2" "Cuboid"  (position (+ -0.017 displacement) 0 Zstart_pmos) (position (+ 0.017 displacement) 0.01  Zend_pmos));
(sdedr:define-analytical-profile-placement "Impl.Channel_pmos_i2" "Impl.ch_pmos_i2" "RefEvalChannel_pmos_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.ch_pmos_i2" DopSub_pmos "PeakPos" 0  "PeakVal" 4e+18 "ValueAtDepth" 1e17 "Depth" 0.016 "Gauss"  "Factor" 1.0)


; - Halo 
; drain halo
(sdedr:define-refinement-window "RefEvalWin_halo_pmos_i2" "Cuboid"   (position  (+  0.0144  displacement) 0.02 Zstart_pmos)   (position (+ 0.03 displacement) 0.04 Zend_pmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo_pmos_i2" "Impl.Haloprof2_pmos_i2" "RefEvalWin_halo_pmos_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2_pmos_i2" DopSub_pmos "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)


; - Halo 
; source halo
(sdedr:define-refinement-window "RefEvalWin_halo2_pmos_i2" "Cuboid"   (position   (+ -0.0144 displacement)  0.02 Zstart_pmos)   (position (+ -0.03 displacement) 0.04 Zend_pmos) )
(sdedr:define-analytical-profile-placement "Impl.Halo2_pmos_i2" "Impl.Haloprof2_pmos_i2" "RefEvalWin_halo2_pmos_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2_pmos_i2" DopSub_pmos "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  drain
; -- base line definitions drain side
(sdedr:define-refinement-window "BaseLine.Ext_pmos_i2" "Rectangle"   (position  (+ Xrox 0.002 displacement)    0.0  Zstart_pmos)   (position (+(* Xmax 1.0) displacement)  0.0 Zend_pmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext_pmos_i2"  "Impl.Extprof_pmos_i2" "BaseLine.Ext_pmos_i2" "Positive" "NoReplace" "Eval")
;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof_pmos_i2"  DopSD_pmos "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  source
; -- base line definitions source side
(sdedr:define-refinement-window "BaseLine.Ext2_pmos_i2" "Rectangle"   (position  (+ (- Xrox_minus 0.002) displacement)   0.0   Zstart_pmos)   (position (+ (* Xmax -1.0) displacement) 0.0 Zend_pmos) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext2_pmos_i2"  "Impl.Extprof2_pmos_i2" "BaseLine.Ext2_pmos_i2" "Negative" "NoReplace" "Eval")

;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof2_pmos_i2"  DopSD_pmos "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; Drain implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD_pmos_i2" "Rectangle"   (position (+ Xsp 0.01 displacement) 0.0  Zstart_pmos)   (position (+ (* Xmax 1.0) displacement)  0.0 Zend_pmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD_pmos_i2"  "Impl.SDprof_pmos_i2" "BaseLine.SD_pmos_i2" "Positive" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof_pmos_i2" DopSD_pmos  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



; Source implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD2_pmos_i2" "Rectangle"   (position (+ (- Xsp_minus 0.01) displacement) 0.0  Zstart_pmos)   (position (+ (* Xmax -1.0) displacement) 0.0 Zend_pmos) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD2_pmos_i2"  "Impl.SDprof2_pmos_i2" "BaseLine.SD2_pmos_i2" "Negative" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof2_pmos_i2" DopSD_pmos  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)


;Body contact doping
(sdedr:define-refeval-window "Ref_Body_Contact_pmos_i2" "Rectangle"  (position (- (+ Xmax_minus displacement) Xsti) 0  Zstart_pmos) (position  (- (+ Xmax_minus displacement) (* 2 Xsti)) 0   (+ Zstart_pmos 0.1)))
(sdedr:define-analytical-profile-placement "Impl.Body_Contact_pmos_i2" "Impl.Body_Contact_pmos_i2" "Ref_Body_Contact_pmos_i2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Body_Contact_pmos_i2" DopSub_pmos "PeakPos" 0  "PeakVal" 5e+18 "ValueAtDepth" 1e18 "Depth" 0.02 "Gauss"  "Factor" 0.2)


;-------------------MESHING---------------------------------------------------
;------------------------MESHING 2----------------------------------------------

; Active 2 - PMOS

(sdedr:define-refinement-window "RWin.Act_pmos_i2" 
 "Cuboid"  
  (position   (+ Xmax_minus displacement)  0.0   Zstart_pmos) 
 (position  (+ (* Xg  -1.2) displacement)  (* XjSD  1.5)    Zend_pmos) )

(sdedr:define-refinement-size "Ref.SiAct_pmos_i2" 
    (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct_pmos_i2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct_pmos_i2" 
 "Ref.SiAct_pmos_i2" "RWin.Act_pmos_i2" )




(sdedr:define-refinement-window "RWin.Act_pmos_part2_i2" 
 "Cuboid"  
 (position   (+ Xmax displacement)  0.0   Zstart_pmos) 
 (position  (+ (* Xg  1.2) displacement)  (* XjSD  1.5)    Zend_pmos) )

(sdedr:define-refinement-size "Ref.SiAct_pmos_part2_i2" 
    (/ Xmax 4.0) (/ XjSD 3.0) (/ Xmax 3.0)
  Gpn      Gpn       Gpn )
(sdedr:define-refinement-function "Ref.SiAct_pmos_part2_i2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct_pmos_part2_i2" 
 "Ref.SiAct_pmos_part2_i2" "RWin.Act_pmos_part2_i2" )




; Poly Gate Multibox
(sdedr:define-refinement-window "MBWindow.Gate_pmos_i2" 
 "Cuboid"
 (position (+ Xg_minus displacement) Ypol   Zstart_pmos) 
 (position (+ Xg displacement) Ygox  Zend_pmos) )

(sdedr:define-multibox-size "MBSize.Gate_pmos_i2" 
  (/ Lg  2.0)  (/ Hpol 2.0)  (/ Lg  2.0) 
  (/ Lg 3.0)   (/ Hpol 4.0) (/ Lg 3.0)
  1.0         -1.35   1.0)
(sdedr:define-multibox-placement "MBPlace.Gate_pmos_i2" 
 "MBSize.Gate_pmos_i2"  "MBWindow.Gate_pmos_i2" )


; GateOx
(sdedr:define-refinement-size "Ref.GOX_pmos_i2" 
 (/ Xmax_well 1.5)  (/ Tox 2.0)    (/ Xmax_well 1.5)
  Gpn           (/ Tox 3.0)  Gpn  )
(sdedr:define-refinement-region "RefPlace.GOX_pmos_i2" 
 "Ref.GOX_pmos_i2" "R.Gateox_pmos_i2" )

; Channel Multibox
(sdedr:define-refinement-window "MBWindow.Channel_pmos_i2" 
 "Cuboid"  
 (position  (+ (* Xg  -1.2) displacement)  0.0   Zstart_pmos) 
 (position (+ (* Xg  1.2) displacement) (* 2.0 XjExt)  Zend_pmos) )
(sdedr:define-multibox-size "MBSize.Channel_pmos_i2" 
  (/ Lg  3.0)  (/ XjSD 4.0)  (/ Lg  3.0)
 (/ Lg 6.0)  1e-3   (/ Lg 5.0)
  1.0     1.35  1.0)
(sdedr:define-multibox-placement "MBPlace.Channel_pmos_i2" 
 "MBSize.Channel_pmos_i2" "MBWindow.Channel_pmos_i2" )



; Body contact
(sdedr:define-refinement-window "RWin.Body_pmos_i2" 
  "Cuboid"
 (position  (- (+ Xmax_minus displacement) Xsti)  0.0   Zstart_pmos) 
 (position  (- (+ Xmax_minus displacement) (* 2 Xsti)) (/ Ysti 4)      (+ Zstart_pmos 0.1)) )

(sdedr:define-refinement-size "Ref.Body_pmos_i2" 
  (/ Xmax 1.3) (/  Xmax 1.3) (/ Xmax 1.3)
 (/ Xmax 2.0) (/ Xmax 2.0) (/ Xmax 2.0))
(sdedr:define-refinement-function "Ref.Body_pmos_i2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.Body_pmos_i2" 
 "Ref.Body_pmos_i2" "RWin.Body_pmos_i2" )


;STI - inv 1

(sdegeo:create-cuboid (position Xmax_minus 0.0 0.0 )  (position Subs_start Ysti Wx_nmos ) "SiO2" "STI2" )
(sdegeo:create-cuboid (position  Xmax 0.0 0.0 )  (position Xmax_sti Ysti Wx_nmos ) "SiO2" "STI3" )
(sdegeo:create-cuboid (position  Xmax_sti 0.0 Z_body )  (position (+ Xmax_sti Xwell) Ysti Wx_nmos ) "SiO2" "STI_body1" )
(sdegeo:create-cuboid (position   (- (+ Xmax_minus displacement) Xsti)  0.0 Z_body )  (position (- (+ Xmax_minus displacement) (* 2 Xsti)) Ysti Wx_nmos ) "SiO2" "STI_bodyn2" )


(sdegeo:create-cuboid (position  Xmax_well 0.0 0.0 )  (position (- (+ Xmax_minus displacement) (* 2 Xsti)) Ysti Wx_nmos ) "SiO2" "STI3_last" )

(sdegeo:create-cuboid (position  (+ displacement Xmax_minus ) 0.0 0.0 )  (position (- (+ Xmax_minus displacement) (* 1 Xsti)) Ysti Wx_nmos ) "SiO2" "STI3_last2" )

(sdegeo:create-cuboid (position  Subs_start  0  Wx_nmos)  (position Subs_end Ysti Zstart_pmos) "SiO2" "sep_STI" )

(sdegeo:create-cuboid (position Xmax_minus 0.0 Zstart_pmos )  (position Subs_start Ysti Zend_pmos ) "SiO2" "STI_pmos1" )
(sdegeo:create-cuboid (position  Xmax 0.0  Zstart_pmos )  (position Xmax_sti Ysti Zend_pmos ) "SiO2" "STI_pmos2" )
(sdegeo:create-cuboid (position   Xmax_sti 0.0 (+ Zstart_pmos 0.1) )  (position (+ Xmax_sti Xwell ) Ysti Zend_pmos ) "SiO2" "STI_pmos2w" )

(sdegeo:create-cuboid (position   (- (+ Xmax_minus displacement) Xsti)  0.0 (+ Zstart_pmos 0.1) )  (position (- (+ Xmax_minus displacement) (* 2 Xsti)) Ysti Zend_pmos ) "SiO2" "STI_bodyp2" )


(sdegeo:create-cuboid (position  Xmax_well 0.0 Zstart_pmos )  (position  (- (+ Xmax_minus displacement) (* 2 Xsti)) Ysti Zend_pmos ) "SiO2" "STIpmos_last" )

(sdegeo:create-cuboid (position  Subs_start  0 Zend_pmos )  (position Subs_end Ysti W_subs) "SiO2" "sti_end" )

(sdegeo:create-cuboid (position  (+ displacement Xmax_minus ) 0.0 Zstart_pmos )  (position (- (+ Xmax_minus displacement) (* 1 Xsti)) Ysti Zend_pmos ) "SiO2" "STI3_last3" )


;--------------Adding STI Inverter 2--------------------

;(sdegeo:create-cuboid (position (+ Xmax_minus displacement) 0.0 0.0 )  (position (+ Subs_start displacement) Ysti Wx_nmos ) "SiO2" "STI2_i2" )
(sdegeo:create-cuboid (position (+ Xmax displacement) 0.0 0.0 )  (position (+ Subs_end displacement) Ysti Wx_nmos ) "SiO2" "STI3_i2n" )

;(sdegeo:create-cuboid (position  (+ Xmax_well displacement) 0.0 0.0 )  (position (+ Subs_end displacement) Ysti Wx_nmos ) "SiO2" "STI3_last_i2" )

(sdegeo:create-cuboid (position  (+ Subs_start displacement)  0  Wx_nmos)  (position (+ Subs_end displacement) Ysti Zstart_pmos) "SiO2" "sep_STI_i2" )

;(sdegeo:create-cuboid (position (+ Xmax_minus displacement) 0.0 Zstart_pmos )  (position (+ Subs_start displacement) Ysti Zend_pmos ) "SiO2" "STI_pmos1_i2" )

(sdegeo:create-cuboid (position   (+ Xmax displacement) 0.0  Zstart_pmos  )  (position (+ Subs_end displacement) Ysti Zend_pmos ) "SiO2" "STI_pmos2w_i2" )

;(sdegeo:create-cuboid (position (+ Xmax displacement) 0.0  Zstart_pmos )  (position (+ Xmax_sti displacement) Ysti Zend_pmos ) "SiO2" "STI_pmos2_i2" )
;(sdegeo:create-cuboid (position  (+ Xmax_well displacement) 0.0 Zstart_pmos )  (position (+ Subs_end displacement) Ysti Zend_pmos ) "SiO2" "STIpmos_last_i2" )
(sdegeo:create-cuboid (position  (+ Subs_start  displacement) 0 Zend_pmos )  (position (+ Subs_end displacement)  Ysti W_subs) "SiO2" "sti_end_i2" )


;Substrate and STI in front of NMOS


;Substrate front
(sdegeo:create-cuboid   (position    Subs_start   Ysub  -0.05 )   (position   (+ Subs_end displacement)   0.0   0 ) "Silicon" "R.Substrate_front" )

(sdedr:define-constant-profile "Const.Subst_front"  DopSub SubDop )
(sdedr:define-constant-profile-region  "PlaceCD.Subst_front"  "Const.Subst_front" "R.Substrate_front" )



(sdegeo:create-cuboid (position Subs_start 0.0 -0.05 )  (position (+ Subs_end displacement) Ysti 0 ) "SiO2" "STI_front" )

; Substrate
(sdedr:define-refinement-size "Ref.Substrate_fr" 
  (* Xmax_sti 2)  (/ Hsub 1.0)  (/ Xmax_sti 1)
(/ Xmax_sti 1.0)  (/ Hsub 2.0)  (/ Xmax_sti 2.0))
(sdedr:define-refinement-function "Ref.Substrate_fr" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-region "RefPlace.Substrate_fr" 
 "Ref.Substrate_fr" "R.Substrate_front" )

