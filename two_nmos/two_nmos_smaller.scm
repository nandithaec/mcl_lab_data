
;; Defined Parameters:

;; Contact Sets:
(sdegeo:define-contact-set "drain1" 4 (color:rgb 0 1 0 )"##" )
(sdegeo:define-contact-set "gate1" 4 (color:rgb 0 0 1 )"##" )
(sdegeo:define-contact-set "substrate" 4 (color:rgb 0 1 1 )"##" )
(sdegeo:define-contact-set "source1" 4 (color:rgb 1 0 0 )"##" )
(sdegeo:define-contact-set "drain2" 4 (color:rgb 1 0 1 )"##" )
(sdegeo:define-contact-set "gate2" 4 (color:rgb 1 1 0 )"##" )
(sdegeo:define-contact-set "source2" 4 (color:rgb 1 1 1 )"##" )

;; Work Planes:
(sde:workplanes-init-scm-binding)

;; Defined ACIS Refinements:
(sde:refinement-init-scm-binding)

;; Reference/Evaluation Windows:
(sdedr:define-refeval-window "RefEvalChannel" "Line" (position -0.017 0 0) (position 0.017 0 0))
(sdedr:define-refeval-window "RefEvalWin_halo" "Rectangle" (position 0.0144 0.02 0) (position 0.03 0.04 0))
(sdedr:define-refeval-window "RefEvalWin_halo2" "Rectangle" (position -0.0144 0.02 0) (position -0.03 0.04 0))
(sdedr:define-refeval-window "BaseLine.Ext" "Line" (position 0.028 0 0) (position 0.18461538461538 0 0))
(sdedr:define-refeval-window "BaseLine.Ext2" "Line" (position -0.028 0 0) (position -0.18461538461538 0 0))
(sdedr:define-refeval-window "BaseLine.SD" "Line" (position 0.07 0 0) (position 0.18461538461538 0 0))
(sdedr:define-refeval-window "BaseLine.SD2" "Line" (position -0.07 0 0) (position -0.18461538461538 0 0))
(sdedr:define-refeval-window "Ref_Deep_p_well" "Line" (position -0.35461538461538 0.5 0) (position 1.0638461538462 0.5 0))
(sdedr:define-refeval-window "Ref_Well_Contact" "Line" (position 0.25461538461538 0 0) (position 0.55461538461538 0 0))
(sdedr:define-refeval-window "RefEvalChannel2" "Line" (position 0.59023076923077 0 0) (position 0.62423076923077 0 0))
(sdedr:define-refeval-window "RefEvalWin_halo3" "Rectangle" (position 0.62163076923077 0.02 0) (position 0.63723076923077 0.04 0))
(sdedr:define-refeval-window "RefEvalWin_halo4" "Rectangle" (position 0.59283076923077 0.02 0) (position 0.57723076923077 0.04 0))
(sdedr:define-refeval-window "BaseLine.Ext3" "Line" (position 0.63523076923077 0 0) (position 0.79184615384615 0 0))
(sdedr:define-refeval-window "BaseLine.Ext4" "Line" (position 0.57923076923077 0 0) (position 0.42261538461538 0 0))
(sdedr:define-refeval-window "BaseLine.SD3" "Line" (position 0.67723076923077 0 0) (position 0.79184615384615 0 0))
(sdedr:define-refeval-window "BaseLine.SD4" "Line" (position 0.53723076923077 0 0) (position 0.42261538461538 0 0))
(sdedr:define-refeval-window "RWin.Act" "Rectangle" (position -0.18461538461538 0 0) (position 0.18461538461538 0.06 0))
(sdedr:define-refeval-window "MBWindow.Gate" "Rectangle" (position -0.02 -0.0868 0) (position 0.02 -0.0018 0))
(sdedr:define-refeval-window "MBWindow.GateOx" "Rectangle" (position -0.024 -0.0018 0) (position 0.024 0 0))
(sdedr:define-refeval-window "MBWindow.Channel" "Rectangle" (position -0.024 0 0) (position 0.024 0.05 0))
(sdedr:define-refeval-window "RWin.GD" "Rectangle" (position 0.006 0 0) (position 0.0235 0.0125 0))
(sdedr:define-refeval-window "RWin.Well" "Rectangle" (position 0.25461538461538 0 0) (position 0.35461538461538 0.08825 0))
(sdedr:define-refeval-window "RWin.Act2" "Rectangle" (position 0.42261538461538 0 0) (position 0.79184615384615 0.06 0))
(sdedr:define-refeval-window "MBWindow.Gate2" "Rectangle" (position 0.58723076923077 -0.0868 0) (position 0.62723076923077 -0.0018 0))
(sdedr:define-refeval-window "MBWindow.GateOx2" "Rectangle" (position 0.58323076923077 -0.0018 0) (position 0.63123076923077 0 0))
(sdedr:define-refeval-window "MBWindow.Channel2" "Rectangle" (position 0.58323076923077 0 0) (position 0.63123076923077 0.05 0))
(sdedr:define-refeval-window "RWin.GD2" "Rectangle" (position 0.61323076923077 0 0) (position 0.63073076923077 0.0125 0))

;; Restore GUI session parameters:
(sde:set-window-position 0 24)
(sde:set-window-size 1920 1004)
(sde:set-window-style "Windows")
(sde:set-background-color 0 127 178 204 204 204)
(sde:scmwin-set-prefs "Bitstream Charter" "Normal" 8 100 )
