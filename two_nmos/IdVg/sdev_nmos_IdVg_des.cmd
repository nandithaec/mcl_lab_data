
File {
   * input files:

  Grid      = "../two_nmos_wider_msh.tdr"
  Parameter = "../models.par"  

  Plot=   "two_nmos_wider_IdVg_1.2.tdr"
  Current="two_nmos_wider_IdVg_1.2.plt"
  Output= "two_nmos_wider_IdVg_1.2.log"

}

Electrode {
   { Name="source1"    Voltage= 0.0  }
   { Name="drain1"     Voltage= 0.0 }
   { Name="gate1"      Voltage= 0.0  }
   { Name="substrate" Voltage= 0.0 }
  { Name="source2"        Voltage=0.0 }
  { Name="drain2"         Voltage=0.0 }
  { Name="gate2"          Voltage=0.0 }

}

*CarrierCarrierScattering
*Piezo( Model(Mobility(hSixBand)))

Physics{
AreaFactor=1.0
*MLDA
eQCVanDort
Mobility(CarrierCarrierScattering  HighFieldsaturation Enormal )
EffectiveIntrinsicDensity(BandGapNarrowing (OldSlotboom))
Recombination ( SRH Auger  Band2Band)
}

Physics(Region="Channel")
{
*Stress-induced Electron Mobility Model
eMultiValley(MLDA)
Piezo
(
Stress = (1e9, -8e7, -8.7e7, 0, 0, 0)
*Stress-induced Electron Mobility Model
Model
(
Mobility(eSubBand  hTensor(Kanda))
DeformationPotential(ekp hkp minimum)
*Strained effective mass
*DOS(eMass hMass)
)
)
}

*Physics (Material="Metal") { MetalWorkFunction (Workfunction= 4.14) }
**Ge(x)Si(1-x)
*Physics (Region="SiGe_sd") { MoleFraction(xfraction=0.3 Grading = 0.0) }



Plot{ 
*--Density and Currents, etc
   eDensity hDensity
   TotalCurrent/Vector eCurrent/Vector hCurrent/Vector
   eMobility hMobility
   eVelocity hVelocity
   eQuasiFermi hQuasiFermi

*--Temperature 
  * eTemperature Temperature * hTemperature

*--Fields and charges
   ElectricField/Vector Potential SpaceCharge

*--Doping Profiles
   Doping DonorConcentration AcceptorConcentration

*--Generation/Recombination
   SRH Band2Band  Auger
  * AvalancheGeneration eAvalancheGeneration hAvalancheGeneration

*--Driving forces
  * eGradQuasiFermi/Vector hGradQuasiFermi/Vector
  * eEparallel hEparallel eENormal hENormal

*--Band structure/Composition
  * BandGap 
  * BandGapNarrowing
  * Affinity
  * ConductionBand ValenceBand
 *  eQuantumPotential
}

Math {
 *-CheckUndefinedModels
   Extrapolate
   Iterations=15
   Notdamped =50
    ExitOnFailure
 RecBoxIntegr
 Method=ILS
 Derivatives
 *Newdiscretization

  RelErrControl
  * ErRef(Electron)=1.e10
  *ErRef(Hole)=1.e10

}



Solve {
   *- Build-up of initial solution:
   NewCurrentPrefix="init"
   Coupled(Iterations=100){ Poisson }
   Coupled{ Poisson Electron }
   
   *- Bias gate to target bias
   Quasistationary(
      InitialStep=0.01 Increment=1.35 
      MinStep=1e-5 MaxStep=1.1
      Goal{ Name="drain1" Voltage= 1.2  }
   ){ Coupled{ Poisson Electron } }
  
   *-  drain voltage sweep
   NewCurrentPrefix=""
   Quasistationary(
      InitialStep=1e-3 Increment=1.35 
      MinStep=1e-5 MaxStep=1.1
      Goal{ Name="gate1" Voltage= 1.2 }
   ){ Coupled{ Poisson Electron }
      CurrentPlot(Time=(Range=(0 1) Intervals=20))
   }
}

