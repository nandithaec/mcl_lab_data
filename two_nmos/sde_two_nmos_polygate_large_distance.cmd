;;The correct command file - feb 24 2015

(sde:clear)

(if (string=? "nMOS" "nMOS") 
 (begin
  (define DopSub "BoronActiveConcentration")
  (define DopSD  "ArsenicActiveConcentration")
;   - Substrate doping level
  (define SubDop  2e17 )   ; [1/cm3]
  (define HaloDop 2e18 ) ; [1/cm3] originally 1.5e18
 )
 (begin 
  (define DopSub "ArsenicActiveConcentration")
  (define DopSD  "BoronActiveConcentration")
;   - Substrate doping level
  (define SubDop  2e17 )   ; [1/cm3]
  (define HaloDop 2e18 ) ; [1/cm3]
 )
)
;----------------------------------------------------------------------
; Setting parameters
; - lateral
(define Lg    0.04)               ; [um] Gate length
(define Lsp   0.04)                   ; [um] Spacer length
(define Lreox 0.006)                 ; [um] Reox Spacer length
(define Ltot (+ Lg (* 2 Lsp) 0.6) )  ; [um] Lateral extend total 

; - layers
(define Hsub 3)   ; [um] Substrate thickness
(define Tox  18e-4) ; [um] Gate oxide thickness oxynitride
(define Hpol 0.07)   ; [um] Poly gate thickness

; - other
;   - spacer rounding
(define fillet-radius 0.08) ; [um] Rounding radius

;   - pn junction resolution
(define Gpn 0.002) ; [um]

(define XjHalo 0.015)  ; [um] Halo depth 0.07 --> 0.04
(define XjExt  0.025) ; [um] Extension depth
(define XjSD   0.04)  ; [um] SD Junction depth
(define Ysilicide  -0.015)  ; [um] Silicide thickness
(define Xsti 0.2)  ; [um] STI thickness
(define Ysti 0.353)  ; [um] STI thickness
(define Xsti_minus (* -1.0 Xsti))  ; [um] STI thickness
(define Xsti2 0)  ; [um] STI2 thickness
(define Xwell 0.1)  ; [um] well contact thickness
;----------------------------------------------------------------------
; Derived quantities
(define Xmax (/ Ltot 3.9))
(define Xmax_minus (* -1.0 Xmax))

(define Xg   (/ Lg   2.0))
(define Xg_minus (* -1.0  Xg))
(define Xsp  (+ Xg   Lsp))
(define Xsp_minus  (* -1.0  Xsp))
(define Xrox (+ Xg   Lreox))
(define Xrox_minus (* -1.0  Xrox))

(define Ysub Hsub)  ;this means Ysub=Hsub
(define Ygox (* Tox -1.0))
(define Ypol (- Ygox Hpol))

(define Xmax_sti (+ Xmax Xsti)) 
(define Xmax_sti_minus (* -1.0  Xmax_sti)) 
(define Lcont (- Xmax_sti Xsp))  


(define Xmax_well (+ Xmax_sti Xsti2 Xwell)) 
(define displacement (+ (* Xmax_sti_minus -1.0) (- Xmax_well 0.002)))

;----------------------------------------------------------------------
; Overlap resolution: New replaces Old
(sdegeo:set-default-boolean "ABA")

;----------------------------------------------------------------------
; Creating substrate region
(sdegeo:create-rectangle 
  (position    Xmax_sti_minus  Ysub  0.0 ) 
  (position    Xmax_well 0.0 0.0 ) "Silicon" "R.Substrate" )

; Creating gate oxide
(sdegeo:create-rectangle 
  (position    Xsp_minus 0.0  0.0 ) 
  (position    Xsp Ygox 0.0 ) 
  "Oxynitride" "R.Gateox"
)

; Creating PolyReox
(sdegeo:create-rectangle 
  (position    Xsp_minus  Ygox 0.0 ) 
  (position    Xsp  Ypol 0.0 ) 
  "Oxide" "R.PolyReox"
)

; Creating spacers regions
(sdegeo:create-rectangle 
  (position   (+ Xg Lreox)  Ygox 0.0 ) 
  (position    Xsp  Ypol 0.0 ) 
  "Si3N4" "R.Spacer1left"
)

; Creating spacers regions
(sdegeo:create-rectangle 
  (position   (- Xg_minus Lreox)  Ygox 0.0 ) 
  (position    Xsp_minus  Ypol 0.0 ) 
  "Si3N4" "R.Spacer1right"
)

; Creating Metal gate
;(sdegeo:create-rectangle   (position    0.0 Ygox 0.0 )   (position    Xg  Ypol 0.0 )   "Metal" "R.Polygate")

;Poly gate
(sdegeo:create-rectangle   (position   Xg_minus Ygox 0.0 )   (position    Xg  Ypol 0.0 )   "PolySi" "R.Polygate")

; Creating silicide for drain
(sdegeo:create-rectangle   (position Xsp 0 0.0 )    (position Xmax Ysilicide 0.0 )   "NickelSilicide" "Silicide_drain" )

; Creating silicide for source
(sdegeo:create-rectangle   (position Xsp_minus 0 0.0 )    (position Xmax_minus Ysilicide 0.0 )   "NickelSilicide" "Silicide_source" )

;Silicide for gate
(sdegeo:create-rectangle   (position Xg_minus Ypol 0.0 )    (position Xg (+ Ypol Ysilicide) 0.0 )   "NickelSilicide" "Silicide_gate" )

;STI
(sdegeo:create-rectangle (position Xmax 0.0 0.0 )  (position Xmax_sti Ysti 0.0 ) "SiO2" "STI1" )
(sdegeo:create-rectangle (position Xmax_minus 0.0 0.0 )  (position Xmax_sti_minus Ysti 0.0 ) "SiO2" "STI2" )
(sdegeo:create-rectangle (position (+ Xmax_sti Xwell) 0.0 0.0 )  (position Xmax_well Ysti 0.0 ) "SiO2" "STI3" )

;Strained Si in channel
;(sdegeo:create-rectangle   (position 0.0 0.0 0.0 )    (position Xg 0.012 0.0 )   "StrainedSilicon" "Channel_strain" )


;Define channel
(sdegeo:create-rectangle 
  (position    Xg_minus  0.0  0.0 ) 
  (position    Xg 0.008 0.0 ) "Silicon" "Channel" )

;----------------------------------------------------------------------
; - rounding spacer
(sdegeo:fillet-2d 
 (find-vertex-id (position Xsp Ypol 0.0 ))
 fillet-radius)

(sdegeo:fillet-2d 
 (find-vertex-id (position Xsp_minus Ypol 0.0 ))
 fillet-radius)

;----------------------------------------------------------------------
(sdegeo:translate-selected (get-body-list) (transform:translation (gvector displacement 0 0)) #t 1)

(sdegeo:create-rectangle   (position   (+ Xmax_sti displacement) 0.0  0.0 )   (position    (+ Xmax_well displacement) Ysub 0.0 ) "Silicon" "Dummy" )

(sdegeo:delete-region (list (car (find-body-id (position (+ Xmax_sti displacement Xwell) 0.1  0)))))

;----------------------------------------------------------------------
; Contact declarations
;;Device 1
(sdegeo:define-contact-set "drain1" 
  4.0  (color:rgb 0.0 1.0 0.0 ) "##")

(sdegeo:define-contact-set "gate1" 
  4.0  (color:rgb 0.0 0.0 1.0 ) "##")

(sdegeo:define-contact-set "substrate"
  4.0  (color:rgb 0.0 1.0 1.0 ) "##")

(sdegeo:define-contact-set "source1" 
  4.0  (color:rgb 1.0 0.0 0.0 ) "##")


;;Device 2

(sdegeo:define-contact-set "drain2" 
  4.0  (color:rgb 1.0 0.0 1.0 ) "##")

(sdegeo:define-contact-set "gate2" 
  4.0  (color:rgb 1.0 1.0 0.0 ) "##")


(sdegeo:define-contact-set "source2" 
  4.0  (color:rgb 1.0 1.0 1.0 ) "##")

;----------------------------------------------------------------------
; Contact settings

;;Device 1
(sdegeo:define-2d-contact 
 (find-edge-id (position (* (+ Xmax Xsp)  0.5)  Ysilicide  0.0))
 "drain1")

(sdegeo:define-2d-contact 
 (find-edge-id (position (* (- Xmax_minus Xsp_minus)  0.5) Ysilicide 0.0))
 "source1")

(sdegeo:define-2d-contact 
 (find-edge-id (position 5e-4 (+ Ypol Ysilicide) 0.0))
 "gate1")

(sdegeo:define-2d-contact 
 (find-edge-id (position (+ Xmax_sti 0.01) 0.0 0.0))
 "substrate")
 

;;Device 2
(sdegeo:define-2d-contact 
 (find-edge-id (position (+ (* (+ Xmax Xsp)  0.5) displacement)  Ysilicide  0.0))
 "drain2")

(sdegeo:define-2d-contact 
 (find-edge-id (position (+ (* (- Xmax_minus Xsp_minus)  0.5) displacement) Ysilicide 0.0))
 "source2")

(sdegeo:define-2d-contact 
 (find-edge-id (position (+ 5e-4 displacement) (+ Ypol Ysilicide) 0.0))
 "gate2")



;----------------------------------------------------------------------
; Separating lumps
(sde:separate-lumps)

;----------------------------------------------------------------------

; Setting region names for 2nd NMOS
(sde:addmaterial 
  (find-body-id (position  (+ 5e-4 displacement)  (- Ysub 0.1) 0.0)) 
  "Silicon" "R.Substrate2")

(sde:addmaterial 
  (find-body-id (position (+ 5e-4 displacement)  (* 0.5 Ygox) 0.0)) 
  "Oxynitride"    "R.Gateox2")

;(sde:addmaterial   (find-body-id (position  5e-4  (- Ygox 0.01)  0.0))   "Metal"  "R.Polygate")
(sde:addmaterial   (find-body-id (position  (+ 5e-4 displacement)  (- Ygox 0.01)  0.0))   "PolySi"  "R.Polygate2")


(sde:addmaterial 
  (find-body-id (position (+ (* -0.5 (+ Xsp Xg)) displacement) (- Ygox 0.01)  0.0)) 
  "Si3N4"   "R.Spacer2left")

(sde:addmaterial 
  (find-body-id (position (+ (*  0.5 (+ Xsp Xg)) displacement) (- Ygox 0.01)  0.0))  
"Si3N4"   "R.Spacer2right")

; -----------------------------------------------------------------------------
;;Doping Profiles:
; Device 1
; Substrate
(sdedr:define-constant-profile "Const.Substrate" 
 DopSub SubDop )
(sdedr:define-constant-profile-region  "PlaceCD.Substrate" 
 "Const.Substrate" "R.Substrate" )

; - Poly
(sdedr:define-constant-profile "Const.Gate"  DopSD 3e20 )(sdedr:define-constant-profile-region "PlaceCD.Gate"  "Const.Gate" "R.Polygate" )



;--Channel implant
(sdedr:define-refeval-window "RefEvalChannel" "Line"  (position -0.017 0 0) (position 0.017 0 0));
(sdedr:define-analytical-profile-placement "Impl.Channel" "Impl.chprof" "RefEvalChannel" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.chprof" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 4e+18 "ValueAtDepth" 1e17 "Depth" 0.016 "Gauss"  "Factor" 1.0)


; - Halo 
; drain halo
(sdedr:define-refinement-window "RefEvalWin_halo" "Rectangle"   (position    0.0144   0.02 0.0)   (position 0.03 0.04 0.0) )
(sdedr:define-analytical-profile-placement "Impl.Halo" "Impl.Haloprof2" "RefEvalWin_halo" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)


; - Halo 
; source halo
(sdedr:define-refinement-window "RefEvalWin_halo2" "Rectangle"   (position    -0.0144   0.02 0.0)   (position -0.03 0.04 0.0) )
(sdedr:define-analytical-profile-placement "Impl.Halo2" "Impl.Haloprof2" "RefEvalWin_halo2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  drain
; -- base line definitions drain side
(sdedr:define-refinement-window "BaseLine.Ext" "Line"   (position  (+ Xrox 0.002)    0.0 0.0)   (position (* Xmax 1.0) 0.0 0.0) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext"  "Impl.Extprof" "BaseLine.Ext" "Positive" "NoReplace" "Eval")
;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  source
; -- base line definitions source side
(sdedr:define-refinement-window "BaseLine.Ext2" "Line"   (position  (- Xrox_minus 0.002)    0.0 0.0)   (position (* Xmax -1.0) 0.0 0.0) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext2"  "Impl.Extprof2" "BaseLine.Ext2" "Negative" "NoReplace" "Eval")

;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof2"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; Drain implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD" "Line"   (position (+ Xsp 0.01) 0.0 0.0)   (position (* Xmax 1.0) 0.0 0.0) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD"  "Impl.SDprof" "BaseLine.SD" "Positive" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



; Source implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD2" "Line"   (position (- Xsp_minus 0.01) 0.0 0.0)   (position (* Xmax -1.0) 0.0 0.0) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD2"  "Impl.SDprof2" "BaseLine.SD2" "Negative" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof2" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)




;Deep p well and retrograde substrate doping
(sdedr:define-refeval-window "Ref_Deep_p_well" "Line"  (position (* Xmax_well -1.0) 0.5 0) (position (* Xmax_well 3) 0.5 0))
(sdedr:define-analytical-profile-placement "Impl.Deep_p_well" "Impl.deep_p_well" "Ref_Deep_p_well" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.deep_p_well" "BoronActiveConcentration" "PeakPos" 0.9  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 1.7 "Gauss"  "Factor" 0.5)



;Well contact doping
(sdedr:define-refeval-window "Ref_Well_Contact" "Line"  (position Xmax_sti 0 0) (position (+ Xmax_well 0.2) 0 0))
(sdedr:define-analytical-profile-placement "Impl.Well_Contact" "Impl.Well_Contact" "Ref_Well_Contact" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Well_Contact" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 5e+18 "ValueAtDepth" 1e18 "Depth" 0.02 "Gauss"  "Factor" 0.2)

;----------------------------------------------------------------------
;Device 2
;----------------------------------------------------------------------


;;Doping Profiles:
; Device 2
; Substrate
(sdedr:define-constant-profile "Const.Substrate2"  DopSub SubDop )
(sdedr:define-constant-profile-region  "PlaceCD.Substrate2"  "Const.Substrate2" "R.Substrate2" )

; - Poly
(sdedr:define-constant-profile "Const.Gate2"  DopSD 3e20 )(sdedr:define-constant-profile-region "PlaceCD.Gate2"  "Const.Gate2" "R.Polygate2" )


;--Channel implant
(sdedr:define-refeval-window "RefEvalChannel2" "Line"  (position (+ -0.017 displacement) 0 0) (position (+ 0.017 displacement) 0 0));
(sdedr:define-analytical-profile-placement "Impl.Channel2" "Impl.chprof2" "RefEvalChannel2" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.chprof2" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 4e+18 "ValueAtDepth" 1e17 "Depth" 0.016 "Gauss"  "Factor" 1.0)


; - Halo 
; drain halo
(sdedr:define-refinement-window "RefEvalWin_halo3" "Rectangle"   (position    (+ 0.0144 displacement)   0.02 0.0)   (position (+ 0.03 displacement) 0.04 0.0) )
(sdedr:define-analytical-profile-placement "Impl.Halo3" "Impl.Haloprof22" "RefEvalWin_halo3" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof22" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)


; - Halo 
; source halo
(sdedr:define-refinement-window "RefEvalWin_halo4" "Rectangle"   (position  (+  -0.0144 displacement)  0.02 0.0)   (position (+ -0.03 displacement) 0.04 0.0) )
(sdedr:define-analytical-profile-placement "Impl.Halo4" "Impl.Haloprof22" "RefEvalWin_halo4" "Positive" "NoReplace" "Eval")
(sdedr:define-gaussian-profile "Impl.Haloprof22" "BoronActiveConcentration" "PeakPos" 0  "PeakVal" 2e+18 "ValueAtDepth" 1e17 "Depth" 0.015 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  drain
; -- base line definitions drain side
(sdedr:define-refinement-window "BaseLine.Ext3" "Line"   (position (+  (+ Xrox 0.002) displacement)    0.0 0.0)   (position (+ (* Xmax 1.0) displacement) 0.0 0.0) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext3"  "Impl.Extprof3" "BaseLine.Ext3" "Positive" "NoReplace" "Eval")
;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof3"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; - Source/Drain extensions  source
; -- base line definitions source side
(sdedr:define-refinement-window "BaseLine.Ext4" "Line"   (position  (+ (- Xrox_minus 0.002) displacement)   0.0 0.0)   (position (+ (* Xmax -1.0) displacement) 0.0 0.0) )
; -- implant placement
(sdedr:define-analytical-profile-placement "Impl.Ext4"  "Impl.Extprof4" "BaseLine.Ext4" "Negative" "NoReplace" "Eval")

;   implant definition
(sdedr:define-gaussian-profile "Impl.Extprof4"  DopSD "PeakPos" 0  "PeakVal" 8e18 "ValueAtDepth" 9e16 "Depth" XjExt
 "Gauss"  "Factor" 0.6)



; Drain implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD3" "Line"   (position (+ (+ Xsp 0.01) displacement) 0.0 0.0)   (position (+ (* Xmax 1.0) displacement) 0.0 0.0) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD3"  "Impl.SDprof3" "BaseLine.SD3" "Positive" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof3" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)



; Source implants
; -- base line definitions
(sdedr:define-refinement-window "BaseLine.SD4" "Line"   (position (+ (- Xsp_minus 0.01) displacement) 0.0 0.0)   (position (+ (* Xmax -1.0) displacement) 0.0 0.0) )
; -- SD implant placement
(sdedr:define-analytical-profile-placement "Impl.SD4"  "Impl.SDprof4" "BaseLine.SD4" "Negative" "NoReplace" "Eval")
; -- implant definition
(sdedr:define-gaussian-profile "Impl.SDprof4" DopSD  "PeakPos" 0  "PeakVal" 1e20 "ValueAtDepth" 2e18  "Depth" XjSD "Gauss"  "Factor" 0.7)




;-------------------MESHING---------------------------------------------------


; Meshing Strategy: Device 1
; Substrate
(sdedr:define-refinement-size "Ref.Substrate" 
  (/ Xmax_sti 4.0)  (/ Hsub 8.0) 
  Gpn            Gpn )
(sdedr:define-refinement-function "Ref.Substrate" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-region "RefPlace.Substrate" 
 "Ref.Substrate" "R.Substrate" )

; Active
(sdedr:define-refinement-window "RWin.Act" 
 "Rectangle"  
 (position   Xmax_minus  0.0   0.0) 
 (position  Xmax (* XjSD  1.5)    0.0) )

(sdedr:define-refinement-size "Ref.SiAct" 
  (/ Lcont 8.0) (/ XjSD 8.0) 
  Gpn            Gpn )
(sdedr:define-refinement-function "Ref.SiAct" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct" 
 "Ref.SiAct" "RWin.Act" )

; Po Gate Multibox
(sdedr:define-refinement-window "MBWindow.Gate" 
 "Rectangle"  
 (position Xg_minus (+ Ypol Ysilicide)  0.0) 
 (position Xg  Ygox  0.0) )

(sdedr:define-multibox-size "MBSize.Gate" 
  (/ Lg  8.0)  (/ Hpol 4.0)
  (/ Lg 10.0)   2e-4 
  1.0         -1.35 )
(sdedr:define-multibox-placement "MBPlace.Gate" 
 "MBSize.Gate"  "MBWindow.Gate" )

; GateOx Multibox
(sdedr:define-refinement-window "MBWindow.GateOx" 
 "Rectangle"  
 (position (* Xg  -1.2)        Ygox  0.0) 
 (position (* Xg  1.2) 0.0  0.0) )

(sdedr:define-multibox-size "MBSize.GateOx" 
  (/ Lg  8.0)  (/ Tox 4.0)
  (/ Lg 10.0)   1e-4 
  1.0         -1.35 )
(sdedr:define-multibox-placement "MBPlace.GateOx" 
 "MBSize.GateOx"  "MBWindow.GateOx" )

; GateOx
(sdedr:define-refinement-size "Ref.GOX" 
  (/ Ltot 4.0)  (/ Tox 4.0) 
  Gpn           (/ Tox 8.0) )
(sdedr:define-refinement-region "RefPlace.GOX" 
 "Ref.GOX" "R.Gateox" )

; Channel Multibox
(sdedr:define-refinement-window "MBWindow.Channel" 
 "Rectangle"  
 (position  (* Xg  -1.2)  0.0   0.0) 
 (position (* Xg  1.2) (* 2.0 XjExt)  0.0) )
(sdedr:define-multibox-size "MBSize.Channel" 
  (/ Lg  8.0)  (/ XjSD 8.0)
  (/ Lg 10.0)   1e-4 
  1.0           1.35 )
(sdedr:define-multibox-placement "MBPlace.Channel" 
 "MBSize.Channel" "MBWindow.Channel" )

; Junctions under the gate
(sdedr:define-refinement-size "Ref.J" 
  (/ XjExt 8.0)  (/ XjExt 8.0) 
  (/ XjExt 10.0) (/ XjExt 10.0))

; Gate-Drain Junction
(sdedr:define-refinement-window "RWin.GD" 
 "Rectangle"  
 (position (- Xrox (* 0.8 XjExt))   0.0   0.0) 
 (position (- Xrox (* 0.1 XjExt))   (* XjExt 0.5)  0.0) )
(sdedr:define-refinement-placement "RefPlace.GJ" 
 "Ref.J" "RWin.GD" )


; Well contact
(sdedr:define-refinement-window "RWin.Well" 
 "Rectangle"  
 (position   Xmax_sti  0.0   0.0) 
 (position  (+ Xmax_sti Xwell) (/ Ysti 4)    0.0) )

(sdedr:define-refinement-size "Ref.Well" 
  (/ Lcont 10.0) (/ XjSD 8.0) 
  Gpn            Gpn )
(sdedr:define-refinement-function "Ref.Well" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.Well" 
 "Ref.Well" "RWin.Well" )

;------------------------MESHING 2----------------------------------------------

;-------------------MESHING---------------------------------------------------


; Meshing Strategy: Device 2
; Substrate
(sdedr:define-refinement-size "Ref.Substrate2" 
  (/ Xmax_sti 4.0)  (/ Hsub 8.0) 
  Gpn            Gpn )
(sdedr:define-refinement-function "Ref.Substrate2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-region "RefPlace.Substrate2" 
 "Ref.Substrate2" "R.Substrate2" )

; Active
(sdedr:define-refinement-window "RWin.Act2" 
 "Rectangle"  
 (position   (+ Xmax_minus displacement)  0.0   0.0) 
 (position  (+ Xmax displacement) (* XjSD  1.5)    0.0) )

(sdedr:define-refinement-size "Ref.SiAct2" 
  (/ Lcont 8.0) (/ XjSD 8.0) 
  Gpn            Gpn )
(sdedr:define-refinement-function "Ref.SiAct2" 
 "DopingConcentration" "MaxTransDiff" 1)
(sdedr:define-refinement-placement "RefPlace.SiAct2" 
 "Ref.SiAct2" "RWin.Act2" )

; Gate Multibox
(sdedr:define-refinement-window "MBWindow.Gate2" 
 "Rectangle"  
 (position (+ Xg_minus displacement) (+ Ypol Ysilicide)  0.0) 
 (position (+ Xg displacement) Ygox  0.0) )

(sdedr:define-multibox-size "MBSize.Gate2" 
  (/ Lg  8.0)  (/ Hpol 4.0)
  (/ Lg 10.0)   2e-4 
  1.0         -1.35 )
(sdedr:define-multibox-placement "MBPlace.Gate2" 
 "MBSize.Gate2"  "MBWindow.Gate2" )

; GateOx Multibox
(sdedr:define-refinement-window "MBWindow.GateOx2" 
 "Rectangle"  
 (position (+ (* Xg  -1.2) displacement)   Ygox  0.0) 
 (position (+ (* Xg  1.2) displacement) 0.0  0.0) )

(sdedr:define-multibox-size "MBSize.GateOx2" 
  (/ Lg  8.0)  (/ Tox 4.0)
  (/ Lg 10.0)   1e-4 
  1.0         -1.35 )
(sdedr:define-multibox-placement "MBPlace.GateOx2" 
 "MBSize.GateOx2"  "MBWindow.GateOx2" )

; GateOx
(sdedr:define-refinement-size "Ref.GOX2" 
  (/ Ltot 4.0)  (/ Tox 4.0) 
  Gpn           (/ Tox 8.0) )
(sdedr:define-refinement-region "RefPlace.GOX2" 
 "Ref.GOX2" "R.Gateox2" )

; Channel Multibox
(sdedr:define-refinement-window "MBWindow.Channel2" 
 "Rectangle"  
 (position  (+ (* Xg  -1.2) displacement)  0.0   0.0) 
 (position (+ (* Xg  1.2) displacement) (* 2.0 XjExt)  0.0) )
(sdedr:define-multibox-size "MBSize.Channel2" 
  (/ Lg  8.0)  (/ XjSD 8.0)
  (/ Lg 10.0)   1e-4 
  1.0           1.35 )
(sdedr:define-multibox-placement "MBPlace.Channel2" 
 "MBSize.Channel2" "MBWindow.Channel2" )

; Junctions under the gate
; Gate-Drain Junction
(sdedr:define-refinement-size "Ref.J2" 
  (/ XjExt 8.0)  (/ XjExt 8.0) 
  (/ XjExt 10.0) (/ XjExt 10.0))


(sdedr:define-refinement-window "RWin.GD2" 
 "Rectangle"  
 (position (+ (- Xrox (* 0.8 XjExt)) displacement)  0.0   0.0) 
 (position (+ (- Xrox (* 0.1 XjExt)) displacement)   (* XjExt 0.5)  0.0) )
(sdedr:define-refinement-placement "RefPlace.GJ2" 
 "Ref.J2" "RWin.GD2" )


;----------------------------------------------------------------------


;(sde:save-model "/home/users/nanditha/sentaurus/65nm_mos/final/two_nmos/two_nmos")
; Build Mesh 
;(sde:build-mesh "snmesh" "-a -c boxmethod" "/home/users/nanditha/sentaurus/65nm_mos/final/nmos/n35_full")

;----------------------------------------------------------------------
; Reflect device

;(system:command "tdx -mtt -x -ren drain=source n35_half_msh n35_full_msh")



