Title "Untitled"

Controls {
}

Definitions {
	Constant "Const.Substrate" {
		Species = "BoronActiveConcentration"
		Value = 2e+17
	}
	Constant "Const.Gate" {
		Species = "ArsenicActiveConcentration"
		Value = 3e+20
	}
	AnalyticalProfile "Impl.chprof" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 4e+18, ValueAtDepth = 1e+17, Depth = 0.016)
		LateralFunction = Gauss(Factor = 1)
	}
	AnalyticalProfile "Impl.Haloprof2" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 2e+18, ValueAtDepth = 1e+17, Depth = 0.015)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.Extprof" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 8e+18, ValueAtDepth = 9e+16, Depth = 0.025)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.Extprof2" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 8e+18, ValueAtDepth = 9e+16, Depth = 0.025)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.SDprof" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 2e+18, Depth = 0.04)
		LateralFunction = Gauss(Factor = 0.7)
	}
	AnalyticalProfile "Impl.SDprof2" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 2e+18, Depth = 0.04)
		LateralFunction = Gauss(Factor = 0.7)
	}
	AnalyticalProfile "Impl.deep_p_well" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0.9, PeakVal = 2e+18, ValueAtDepth = 1e+17, Depth = 1.7)
		LateralFunction = Gauss(Factor = 0.5)
	}
	AnalyticalProfile "Impl.Well_Contact" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 5e+18, ValueAtDepth = 1e+18, Depth = 0.02)
		LateralFunction = Gauss(Factor = 0.2)
	}
	Constant "Const.Substrate2" {
		Species = "BoronActiveConcentration"
		Value = 2e+17
	}
	Constant "Const.Gate2" {
		Species = "ArsenicActiveConcentration"
		Value = 3e+20
	}
	AnalyticalProfile "Impl.chprof2" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 4e+18, ValueAtDepth = 1e+17, Depth = 0.016)
		LateralFunction = Gauss(Factor = 1)
	}
	AnalyticalProfile "Impl.Haloprof22" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 2e+18, ValueAtDepth = 1e+17, Depth = 0.015)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.Extprof3" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 8e+18, ValueAtDepth = 9e+16, Depth = 0.025)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.Extprof4" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 8e+18, ValueAtDepth = 9e+16, Depth = 0.025)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.SDprof3" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 2e+18, Depth = 0.04)
		LateralFunction = Gauss(Factor = 0.7)
	}
	AnalyticalProfile "Impl.SDprof4" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 2e+18, Depth = 0.04)
		LateralFunction = Gauss(Factor = 0.7)
	}
	AnalyticalProfile "Impl.Well2_Contact" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 5e+18, ValueAtDepth = 1e+18, Depth = 0.02)
		LateralFunction = Gauss(Factor = 0.2)
	}
	Constant "Const.Substrate3" {
		Species = "BoronActiveConcentration"
		Value = 2e+17
	}
	Constant "Const.Gate3" {
		Species = "ArsenicActiveConcentration"
		Value = 3e+20
	}
	AnalyticalProfile "Impl.chprof3" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 4e+18, ValueAtDepth = 1e+17, Depth = 0.016)
		LateralFunction = Gauss(Factor = 1)
	}
	AnalyticalProfile "Impl.Haloprof5" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 2e+18, ValueAtDepth = 1e+17, Depth = 0.015)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.Haloprof6" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 2e+18, ValueAtDepth = 1e+17, Depth = 0.015)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.Extprof5" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 8e+18, ValueAtDepth = 9e+16, Depth = 0.025)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.Extprof6" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 8e+18, ValueAtDepth = 9e+16, Depth = 0.025)
		LateralFunction = Gauss(Factor = 0.6)
	}
	AnalyticalProfile "Impl.SDprof5" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 2e+18, Depth = 0.04)
		LateralFunction = Gauss(Factor = 0.7)
	}
	AnalyticalProfile "Impl.SDprof6" {
		Species = "ArsenicActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 1e+20, ValueAtDepth = 2e+18, Depth = 0.04)
		LateralFunction = Gauss(Factor = 0.7)
	}
	AnalyticalProfile "Impl.Well3_Contact" {
		Species = "BoronActiveConcentration"
		Function = Gauss(PeakPos = 0, PeakVal = 5e+18, ValueAtDepth = 1e+18, Depth = 0.02)
		LateralFunction = Gauss(Factor = 0.2)
	}
	Refinement "Ref.Substrate" {
		MaxElementSize = ( 0.071153846153846 0.375 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.SiAct" {
		MaxElementSize = ( 0.028076923076923 0.005 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.GOX" {
		MaxElementSize = ( 0.18 0.00045 )
		MinElementSize = ( 0.002 0.000225 )
	}
	Refinement "Ref.Well" {
		MaxElementSize = ( 0.022461538461538 0.005 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.Substrate2" {
		MaxElementSize = ( 0.071153846153846 0.375 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.SiAct2" {
		MaxElementSize = ( 0.028076923076923 0.005 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.GOX2" {
		MaxElementSize = ( 0.18 0.00045 )
		MinElementSize = ( 0.002 0.000225 )
	}
	Refinement "Ref.Well2" {
		MaxElementSize = ( 0.022461538461538 0.005 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.Substrate3" {
		MaxElementSize = ( 0.071153846153846 0.375 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.SiAct3" {
		MaxElementSize = ( 0.028076923076923 0.005 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Refinement "Ref.GOX3" {
		MaxElementSize = ( 0.18 0.00045 )
		MinElementSize = ( 0.002 0.000225 )
	}
	Refinement "Ref.Well3" {
		MaxElementSize = ( 0.022461538461538 0.005 )
		MinElementSize = ( 0.002 0.002 )
		RefineFunction = MaxTransDiff(Variable = "DopingConcentration",Value = 1)
	}
	Multibox "MBSize.Gate"
 {
		MaxElementSize = ( 0.005 0.0275 )
		MinElementSize = ( 0.004 0.0002 )
		Ratio = ( 1 -1.35 )
	}
	Multibox "MBSize.Channel"
 {
		MaxElementSize = ( 0.005 0.005 )
		MinElementSize = ( 0.004 0.0001 )
		Ratio = ( 1 1.35 )
	}
	Multibox "MBSize.Gate2"
 {
		MaxElementSize = ( 0.005 0.0275 )
		MinElementSize = ( 0.004 0.0002 )
		Ratio = ( 1 -1.35 )
	}
	Multibox "MBSize.Channel2"
 {
		MaxElementSize = ( 0.005 0.005 )
		MinElementSize = ( 0.004 0.0001 )
		Ratio = ( 1 1.35 )
	}
	Multibox "MBSize.Gate3"
 {
		MaxElementSize = ( 0.005 0.0275 )
		MinElementSize = ( 0.004 0.0002 )
		Ratio = ( 1 -1.35 )
	}
	Multibox "MBSize.Channel3"
 {
		MaxElementSize = ( 0.005 0.005 )
		MinElementSize = ( 0.004 0.0001 )
		Ratio = ( 1 1.35 )
	}
}

Placements {
	Constant "PlaceCD.Substrate" {
		Reference = "Const.Substrate"
		EvaluateWindow {
			Element = region ["R.Substrate"]
		}
	}
	Constant "PlaceCD.Gate" {
		Reference = "Const.Gate"
		EvaluateWindow {
			Element = region ["R.Polygate"]
		}
	}
	AnalyticalProfile "Impl.Channel" {
		Reference = "Impl.chprof"
		ReferenceElement {
			Element = Line [(-0.017 0) (0.017 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Halo" {
		Reference = "Impl.Haloprof2"
		ReferenceElement {
			Element = Rectangle [(0.0144 0.02) (0.03 0.04)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Halo2" {
		Reference = "Impl.Haloprof2"
		ReferenceElement {
			Element = Rectangle [(-0.0144 0.02) (-0.03 0.04)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Ext" {
		Reference = "Impl.Extprof"
		ReferenceElement {
			Element = Line [(0.028 0) (0.184615384615 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Ext2" {
		Reference = "Impl.Extprof2"
		ReferenceElement {
			Element = Line [(-0.028 0) (-0.184615384615 0)]
			Direction = negative
		}
	}
	AnalyticalProfile "Impl.SD" {
		Reference = "Impl.SDprof"
		ReferenceElement {
			Element = Line [(0.07 0) (0.184615384615 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.SD2" {
		Reference = "Impl.SDprof2"
		ReferenceElement {
			Element = Line [(-0.07 0) (-0.184615384615 0)]
			Direction = negative
		}
	}
	AnalyticalProfile "Impl.Deep_p_well" {
		Reference = "Impl.deep_p_well"
		ReferenceElement {
			Element = Line [(-0.384615384615 0.5) (1.923076923077 0.5)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Well_Contact" {
		Reference = "Impl.Well_Contact"
		ReferenceElement {
			Element = Line [(0.284615384615 0) (0.404615384615 0)]
			Direction = positive
		}
	}
	Constant "PlaceCD.Substrate2" {
		Reference = "Const.Substrate2"
		EvaluateWindow {
			Element = region ["R.Substrate2"]
		}
	}
	Constant "PlaceCD.Gate2" {
		Reference = "Const.Gate2"
		EvaluateWindow {
			Element = region ["R.Polygate2"]
		}
	}
	AnalyticalProfile "Impl.Channel2" {
		Reference = "Impl.chprof2"
		ReferenceElement {
			Element = Line [(0.650230769231 0) (0.684230769231 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Halo3" {
		Reference = "Impl.Haloprof22"
		ReferenceElement {
			Element = Rectangle [(0.681630769231 0.02) (0.697230769231 0.04)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Halo4" {
		Reference = "Impl.Haloprof22"
		ReferenceElement {
			Element = Rectangle [(0.652830769231 0.02) (0.637230769231 0.04)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Ext3" {
		Reference = "Impl.Extprof3"
		ReferenceElement {
			Element = Line [(0.695230769231 0) (0.851846153846 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Ext4" {
		Reference = "Impl.Extprof4"
		ReferenceElement {
			Element = Line [(0.639230769231 0) (0.482615384615 0)]
			Direction = negative
		}
	}
	AnalyticalProfile "Impl.SD3" {
		Reference = "Impl.SDprof3"
		ReferenceElement {
			Element = Line [(0.737230769231 0) (0.851846153846 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.SD4" {
		Reference = "Impl.SDprof4"
		ReferenceElement {
			Element = Line [(0.597230769231 0) (0.482615384615 0)]
			Direction = negative
		}
	}
	AnalyticalProfile "Impl.Well2_Contact" {
		Reference = "Impl.Well2_Contact"
		ReferenceElement {
			Element = Line [(0.951846153846 0) (1.071846153846 0)]
			Direction = positive
		}
	}
	Constant "PlaceCD.Substrate3" {
		Reference = "Const.Substrate3"
		EvaluateWindow {
			Element = region ["R.Substrate3"]
		}
	}
	Constant "PlaceCD.Gate3" {
		Reference = "Const.Gate3"
		EvaluateWindow {
			Element = region ["R.Polygate3"]
		}
	}
	AnalyticalProfile "Impl.Channel3" {
		Reference = "Impl.chprof3"
		ReferenceElement {
			Element = Line [(1.317461538462 0) (1.351461538462 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Halo5" {
		Reference = "Impl.Haloprof5"
		ReferenceElement {
			Element = Rectangle [(1.348861538462 0.02) (1.364461538462 0.04)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Halo6" {
		Reference = "Impl.Haloprof6"
		ReferenceElement {
			Element = Rectangle [(1.320061538462 0.02) (1.304461538462 0.04)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Ext5" {
		Reference = "Impl.Extprof5"
		ReferenceElement {
			Element = Line [(1.362461538462 0) (1.519076923077 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.Ext6" {
		Reference = "Impl.Extprof6"
		ReferenceElement {
			Element = Line [(1.306461538462 0) (1.149846153846 0)]
			Direction = negative
		}
	}
	AnalyticalProfile "Impl.SD5" {
		Reference = "Impl.SDprof5"
		ReferenceElement {
			Element = Line [(1.404461538462 0) (1.519076923077 0)]
			Direction = positive
		}
	}
	AnalyticalProfile "Impl.SD6" {
		Reference = "Impl.SDprof6"
		ReferenceElement {
			Element = Line [(1.264461538462 0) (1.149846153846 0)]
			Direction = negative
		}
	}
	AnalyticalProfile "Impl.Well3_Contact" {
		Reference = "Impl.Well3_Contact"
		ReferenceElement {
			Element = Line [(1.619076923077 0) (1.739076923077 0)]
			Direction = positive
		}
	}
	Refinement "RefPlace.Substrate" {
		Reference = "Ref.Substrate"
		RefineWindow = region ["R.Substrate"]
	}
	Refinement "RefPlace.SiAct" {
		Reference = "Ref.SiAct"
		RefineWindow = Rectangle [(-0.184615384615 0) (0.184615384615 0.06)]
	}
	Refinement "RefPlace.GOX" {
		Reference = "Ref.GOX"
		RefineWindow = region ["R.Gateox"]
	}
	Refinement "RefPlace.Well" {
		Reference = "Ref.Well"
		RefineWindow = Rectangle [(0.284615384615 0) (0.384615384615 0.08825)]
	}
	Refinement "RefPlace.Substrate2" {
		Reference = "Ref.Substrate2"
		RefineWindow = region ["R.Substrate2"]
	}
	Refinement "RefPlace.SiAct2" {
		Reference = "Ref.SiAct2"
		RefineWindow = Rectangle [(0.482615384615 0) (0.851846153846 0.06)]
	}
	Refinement "RefPlace.GOX2" {
		Reference = "Ref.GOX2"
		RefineWindow = region ["R.Gateox2"]
	}
	Refinement "RefPlace.Well2" {
		Reference = "Ref.Well2"
		RefineWindow = Rectangle [(0.951846153846 0) (1.051846153846 0.08825)]
	}
	Refinement "RefPlace.Substrate3" {
		Reference = "Ref.Substrate3"
		RefineWindow = region ["R.Substrate3"]
	}
	Refinement "RefPlace.SiAct3" {
		Reference = "Ref.SiAct3"
		RefineWindow = Rectangle [(1.149846153846 0) (1.519076923077 0.06)]
	}
	Refinement "RefPlace.GOX3" {
		Reference = "Ref.GOX3"
		RefineWindow = region ["R.Gateox3"]
	}
	Refinement "RefPlace.Well3" {
		Reference = "Ref.Well3"
		RefineWindow = Rectangle [(1.619076923077 0) (1.719076923077 0.08825)]
	}
	Multibox "MBPlace.Gate" {
		Reference = "MBSize.Gate"
		RefineWindow = Rectangle [(-0.02 -0.1268) (0.02 -0.0018)]
	}
	Multibox "MBPlace.Channel" {
		Reference = "MBSize.Channel"
		RefineWindow = Rectangle [(-0.024 0) (0.024 0.05)]
	}
	Multibox "MBPlace.Gate2" {
		Reference = "MBSize.Gate2"
		RefineWindow = Rectangle [(0.64723076923077 -0.1268) (0.68723076923077 -0.0018)]
	}
	Multibox "MBPlace.Channel2" {
		Reference = "MBSize.Channel2"
		RefineWindow = Rectangle [(0.64323076923077 0) (0.69123076923077 0.05)]
	}
	Multibox "MBPlace.Gate3" {
		Reference = "MBSize.Gate3"
		RefineWindow = Rectangle [(1.3144615384615 -0.1268) (1.3544615384615 -0.0018)]
	}
	Multibox "MBPlace.Channel3" {
		Reference = "MBSize.Channel3"
		RefineWindow = Rectangle [(1.3104615384615 0) (1.3584615384615 0.05)]
	}
}

