# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

*This contains the device simulation raw data: device command files simulation template and csv files, 
python scripts to run the Sentaurus device simulations and analyze the data

### How do I get set up? ###

Python scripts have description inside them on how to use them

###Device commands###
Commands:
Tool Sentaurus Structure Editor: sde - to make the device- sde -e <cmd file>
Or copy- paste the commands in the command window after opening sde
Once the device is made, Build mesh - to obtain tdr. Tdr will be used to run Sdevice simulation

sdevice <cmd file> - to run simulation
svisual & - to view the device
inspect & - to view current or transient plots

Scripts written by me to automate
Python_generate_sdevice_runsim.py or python_current.py needs to be copied in every folder where 
the sdevice command files need to be generated. It takes a template file as input and a csv file
having different parameters as another input. It generates sdev files and runs sim
Python_runboth.py - combines both python_post-process_250p_limit.py and python_combine_2csv.py. 
The first one extracts charge values from the plt files. The second one, combines the input csv 
file and this charge values column by column.
Or copy- paste the commands in the command window after opening sde
Once the device is made, Build mesh - to obtain tdr. Tdr will be used to run sdevice simulation


### Who do I talk to? ###

contact me at: nanditha@ee.iitb.ac.in (available temporarily) or at nanditha.ec@gmail.com
Dept of EE, IIT Bombay, Mumbai, India